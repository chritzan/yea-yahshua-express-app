import 'package:flutter/material.dart';

import 'package:yahshua_express_app/login/login_for_launch/userlogin.dart';
import 'package:yahshua_express_app/login/login_for_launch/adminlogin.dart';

class EntryPoint extends StatefulWidget {
  static String route = "entry-point";
  const EntryPoint({Key? key}) : super(key: key);

  @override
  State<EntryPoint> createState() => _EntryPointState();
}

class _EntryPointState extends State<EntryPoint> {
  //bool for obscure text

  bool isLoading = false;
  final double opacity = 0.2;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return MaterialApp(
      theme: ThemeData.light().copyWith(
        hintColor: Colors.white,
      ),
      home: Scaffold(
        backgroundColor: Colors.white,
        body: isLoading
            ? Center(
                child: SizedBox(
                  height: size.height / 20,
                  width: size.height / 20,
                  child: const CircularProgressIndicator(
                    color: Colors.amber,
                  ),
                ),
              )
            : Center(
                child: Stack(
                  children: [
                    Container(
                      constraints: const BoxConstraints.expand(),
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(opacity),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20),
                        ),
                        border: Border.all(
                          width: 1.5,
                          color: Colors.white.withOpacity(0.2),
                        ),
                      ),
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            left: 10,
                            right: 10,
                            top: 300,
                          ),
                          child: Container(
                            color: const Color(0xFFFFD54F),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  const SizedBox(height: 85),
                                  ElevatedButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const AdminLoginScreen()));
                                      },
                                      child: const Text('Admin')),
                                  const Spacer(),
                                  ElevatedButton(
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    const UserLoginScreen()));
                                      },
                                      child: const Text('User')),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
