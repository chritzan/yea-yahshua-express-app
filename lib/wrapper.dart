// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:yahshua_express_app/home/admin_home.dart';
// import 'package:yahshua_express_app/login/app_entry_point.dart';
// // import 'package:yahshua_express_app/login/user_login.dart';


// // ignore: use_key_in_widget_constructors
// class Wrapper extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final authService = Provider.of<AuthService>(context);
//     return StreamBuilder<User?>(
//       stream: authService.user,
//       builder: (_, AsyncSnapshot<User?> snapshot) {
//         if (snapshot.connectionState == ConnectionState.active) {
//           final User? user = snapshot.data;
//           return user == null ? const EntryPoint() : const AdminHome();
//         } else {
//           return const Scaffold(
//             body: Center(
//               child: CircularProgressIndicator(),
//             ),
//           );
//         }
//       },
//     );
//   }
// }
