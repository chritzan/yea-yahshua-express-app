import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'package:yahshua_express_app/admin_menu/activity/activity.dart';
import 'package:yahshua_express_app/admin_menu/dashboard/dashboard.dart';
import 'package:yahshua_express_app/admin_menu/help/help.dart';
import 'package:yahshua_express_app/admin_menu/item/items_list.dart';
import 'package:yahshua_express_app/admin_menu/scheduling/scheduling.dart';
import 'package:yahshua_express_app/admin_menu/setting/add_items_units/add_items.dart';
import 'package:yahshua_express_app/admin_menu/setting/add_items_units/add_units.dart';
import 'package:yahshua_express_app/admin_menu/setting/item_setting.dart';
import 'package:yahshua_express_app/admin_menu/setting/setting.dart';
import 'package:yahshua_express_app/admin_menu/setting/user_setting.dart';
import 'package:yahshua_express_app/admin_menu/setting/user_setting_rov.dart';
import 'package:yahshua_express_app/admin_menu/user/add_user_account.dart/add_user.dart';
import 'package:yahshua_express_app/admin_menu/user/user_accounts.dart';
import 'package:yahshua_express_app/home/admin_home.dart';
import 'package:yahshua_express_app/home/user_home.dart';
import 'package:yahshua_express_app/login/admin_login.dart';
import 'package:yahshua_express_app/login/app_entry_point.dart';
import 'package:yahshua_express_app/login/user_login.dart';

import 'package:yahshua_express_app/user_menu/deliver/deliver.dart';
import 'package:yahshua_express_app/user_menu/deliver/delivered_success.dart';

import 'package:yahshua_express_app/user_menu/deliver/re_schedule_delivery/chat_deliver.dart';
import 'package:yahshua_express_app/user_menu/pick_up/pick_up.dart';
import 'package:yahshua_express_app/user_menu/request/request.dart';
import 'package:yahshua_express_app/user_menu/request/request_success.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}
//  void main() {
//    runApp(const MyApp());
//  }

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();

    upload();
  }

  Future upload() async {
    //initialize firebase
    await Firebase.initializeApp();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'YEA (Yahshua Express App)',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        EntryPoint.route: (context) => const EntryPoint(),
        //admin-routes
        AdminLoginScreen.route: (context) => const AdminLoginScreen(),

        AdminHome.route: (context) => const AdminHome(),
        Activity.route: (context) => const Activity(),
        Dashboard.route: (context) => const Dashboard(),
        HelpMenu.route: (context) => const HelpMenu(),
        ItemsPage.route: (context) => const ItemsPage(),
        Scheduling.route: (context) => const Scheduling(),

        //item-setting-routes
        ItemSetting.route: (context) => const ItemSetting(),
        AddItemsWidget.route: (context) => const AddItemsWidget(),
        AddUnitWidget.route: (context) => const AddUnitWidget(),

        Setting.route: (context) => const Setting(),

        //user-setting-routes
        UserSetting.route: (context) => const UserSetting(),
        UserSettingRov.route: (context) => const UserSettingRov(),

        //user-accounts-routes
        AccountHome.route: (context) => const AccountHome(),
        AddUserWidget.route: (context) => const AddUserWidget(),

        // UpdateUserAccount.route: (context) => const UpdateUserAccount(),

        //user-routes
        UserLoginScreen.route: (context) => const UserLoginScreen(),
        UserScreen.route: (context) => const UserScreen(),

        //Delivery Routes
        DeliveryScreen.route: (context) => const DeliveryScreen(),
        DeliveredSuccess.route: (context) => const DeliveredSuccess(),
        ChatDeliveryScreen.route: (context) => const ChatDeliveryScreen(),

        //Pick-up Routes
        PickUp.route: (context) => const PickUp(),

        //Request Routes
        RequestPage.route: (context) => const RequestPage(),
        RequestSuccess.route: (context) => const RequestSuccess(),
        //  RequestData.route: (context) => const RequestData(),
      },
      // initialRoute: AdminHome.route,

      home: const EntryPoint(),
    );
  }
}
