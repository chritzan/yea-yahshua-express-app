import 'package:flutter/material.dart';
import 'package:yahshua_express_app/login/app_entry_point.dart';

class UserNavigationWidget extends StatefulWidget {
  const UserNavigationWidget({Key? key}) : super(key: key);

  @override
  _UserNavigationWidgetState createState() => _UserNavigationWidgetState();
}

class _UserNavigationWidgetState extends State<UserNavigationWidget> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Material(
        color: const Color.fromRGBO(150, 175, 05, 1),
        child: ListView(
          children: <Widget>[
            Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                const SizedBox(
                  height: 30,
                  child: Text('Coming Soon!',
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.black,
                      )),
                ),
                TextButton(
                    child: Row(
                      children: const [
                        Icon(Icons.logout_outlined, color: Colors.red),
                        Text(
                          "Logout",
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const EntryPoint()),
                        (Route<dynamic> route) => false,
                      );
                    }),
                const SizedBox(height: 24),
                const Divider(color: Colors.white70),
                const SizedBox(height: 24),
                Center(
                  child: Column(
                    children: const [
                      Text('Powered by Yahshua Systech &'),
                      Text('YAHSHUA Early Career Program'),
                      Text('Batch 5'),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
