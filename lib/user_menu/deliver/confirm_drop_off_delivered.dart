import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:yahshua_express_app/user_menu/deliver/confirm_drop_off_delivered_receiver.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:camera/camera.dart';

class ConfirmDropOffDelivered extends StatefulWidget {
  static String route = 'confirm-dropoff-delivered';
  const ConfirmDropOffDelivered({
    Key? key,
    required this.requester,
    required this.activity,
    required this.datetime,
    required this.runner,
    required this.item,
    required this.location,
  }) : super(key: key);
  final String activity, datetime, runner, item, location, requester;

  @override
  State<ConfirmDropOffDelivered> createState() =>
      _ConfirmDropOffDeliveredState();
}

class _ConfirmDropOffDeliveredState extends State<ConfirmDropOffDelivered> {
  final imagePicker = ImagePicker();
  List<CameraDescription>? cameras; //list out the camera available
  CameraController? controller; //controller for camera
  XFile? image; //for caputred image
  String? downloadURL;
  UploadTask? uploadTask;

  final double opacity = 0.2;

  @override
  void initState() {
    loadCamera();

    super.initState();
  }

  loadCamera() async {
    cameras = await availableCameras();
    if (cameras != null) {
      controller = CameraController(cameras![0], ResolutionPreset.max);
      //cameras[0] = first camera, change to 1 to another camera

      controller!.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    } else {
      // ignore: avoid_print
      print("NO any camera found");
    }
  }

  Future uploadFile() async {
    final path = 'delivery/${image!.name}';
    final file = File(image!.path);

    final ref = FirebaseStorage.instance.ref().child(path);

    setState(() {
      uploadTask = ref.putFile(file);
      if (file != null) {
        File(image!.path);
      } else {
        showSnackBar("No File selected", const Duration(milliseconds: 400));
      }
    });

    final snapshot = await uploadTask!.whenComplete(() {});

    final urlDownload = await snapshot.ref.getDownloadURL();
    // ignore: avoid_print
    print('Download Link: $urlDownload');

    setState(() {
      uploadTask = null;
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Column(
            children: const [
              Text('Confirm Drop-off'),
              Text('Item #_'),
            ],
          ),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                ElevatedButton(
                  child: const Text(
                    'Take A Picture',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              content: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: SizedBox(
                                          height: 300,
                                          width: 400,
                                          child: controller == null
                                              ? const Center(
                                                  child:
                                                      Text("Loading Camera..."))
                                              : !controller!.value.isInitialized
                                                  ? const Center(
                                                      child:
                                                          CircularProgressIndicator(),
                                                    )
                                                  : CameraPreview(controller!)),
                                    ),
                                    ElevatedButton.icon(
                                        onPressed: () async {
                                          try {
                                            if (controller != null) {
                                              //check if contrller is not null
                                              if (controller!
                                                  .value.isInitialized) {
                                                //check if controller is initialized
                                                image = await controller!
                                                    .takePicture(); //capture image
                                                setState(() {
                                                  //update UI
                                                });
                                              }
                                            }
                                          } catch (e) {
                                            // ignore: avoid_print
                                            print(e); //show error
                                          }
                                          Navigator.of(context).pop();
                                        },
                                        icon: const Icon(Icons.camera),
                                        label: const Text('Capture')),
                                  ],
                                ),
                              ),
                            ));
                  },
                ),
                SizedBox(
                  // width: 300,
                  // height: 350,
                  //show captured image
                  // padding: const EdgeInsets.all(30),
                  child: image == null
                      ? const Text("No image captured")
                      : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.file(
                            File(image!.path),
                            // width: 400,
                            height: 200,
                          ),
                        ),
                  //display captured image
                ),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                    onPressed: () {
                      uploadFile();
                    },
                    child: const Text('Upload')),
                buildProgress(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Icon(Icons.circle),
                    const Text('DRAW SIGNATURE'),
                    IconButton(
                      onPressed: openDialog,
                      icon: const Icon(Icons.edit),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 50,
                  width: 100,
                  child: Card(
                    child: Text('For Signature'),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: const Text('Upload'),
                ),
                const SizedBox(
                  height: 25,
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => ConfirmDropOffDeliveredReceiver(
                            requester: widget.requester,
                            activity: widget.activity,
                            datetime: widget.datetime,
                            runner: widget.runner,
                            item: widget.item,
                            location: widget.location)));
                  },
                  child: const Text('CONFIRM DROP-OFF'),
                ),
              ],
            ),
          ),
        ),
      );

  Future<String?> openDialog() => showDialog<String>(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('For Signature'),
          content: SingleChildScrollView(
            child: Column(),
          ),
        ),
      );

  Widget buildProgress() => StreamBuilder<TaskSnapshot>(
      stream: uploadTask?.snapshotEvents,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final data = snapshot.data!;
          double progress = data.bytesTransferred / data.totalBytes;

          return SizedBox(
            height: 50,
            child: Stack(
              fit: StackFit.expand,
              children: [
                LinearProgressIndicator(
                  value: progress,
                  backgroundColor: Colors.amber,
                  color: Colors.green,
                ),
                Center(
                  child: Text(
                    '${(100 * progress).roundToDouble()}%',
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          );
        } else {
          return const SizedBox(
            height: 50,
          );
        }
      });

  showSnackBar(String snackText, Duration d) {
    final snackBar = SnackBar(content: Text(snackText), duration: d);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
