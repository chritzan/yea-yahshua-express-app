import 'package:flutter/material.dart';

class ChatDeliveryScreen extends StatefulWidget {
  static String route = 'chat-delivery';
  const ChatDeliveryScreen({Key? key}) : super(key: key);

  @override
  _ChatDeliveryScreenState createState() => _ChatDeliveryScreenState();
}

class _ChatDeliveryScreenState extends State<ChatDeliveryScreen> {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: Row(
            children: const [Text('_Requester'), Icon(Icons.call)],
          ),
          centerTitle: true,
        ),
        body: Center(
            child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 250,
                child: Card(
                  shape: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  margin: const EdgeInsets.all(20),
                  shadowColor: Colors.blue,
                  elevation: 10,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      const Text('A Blessed Day, "_!"'),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text('You have a delivery request from "_"'),
                      const Text('with the following details: '),
                      const Text('ITEMS: "_" '),
                      const Text('Location: "_"'),
                      const Text('Date: "_"'),
                      const Text('Time: "_"'),
                      Row(
                        children: [
                          const Spacer(),
                          IconButton(
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, ChatDeliveryScreen.route);
                              },
                              icon: const Icon(Icons.forward)),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        )),
      );
}
