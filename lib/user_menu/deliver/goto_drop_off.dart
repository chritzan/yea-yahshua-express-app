import 'package:flutter/material.dart';
import 'package:yahshua_express_app/user_menu/deliver/drop_off.dart';

class GotoDropOff extends StatefulWidget {
  static String route = 'goto-drop-off';
  const GotoDropOff({
    Key? key,
    required this.requester,
    required this.activity,
    required this.datetime,
    required this.runner,
    required this.item,
    required this.location,
  }) : super(key: key);
  final String activity, datetime, runner, item, location, requester;
  @override
  State<GotoDropOff> createState() => _GotoDropOffState();
}

class _GotoDropOffState extends State<GotoDropOff> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Go to Drop-off'),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const Text(
                  'Receiver Details',
                  style: TextStyle(
                    fontSize: 10,
                  ),
                ),
                Row(
                  children: [
                    Text(widget.requester + 'Receiver'),
                    const Spacer(),
                    const Icon(Icons.dialer_sip),
                  ],
                ),
                Row(
                  children: const [
                    Icon(Icons.location_on),
                    Text('Kauswagan, Cagayan De Oro City'),
                  ],
                ),
                const Divider(
                  color: Colors.grey,
                ),
                const Text('Arrived at _:_'),
                const SizedBox(
                  height: 100,
                  child: Text('For Map vIew'),
                ),
                const SizedBox(
                  height: 25,
                ),
                ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => DropOff(
                            requester: widget.requester,
                            activity: widget.activity,
                            datetime: widget.datetime,
                            runner: widget.runner,
                            item: widget.item,
                            location: widget.location)));
                  },
                  child: const Text('Arrived At Drop-OFF'),
                )
              ],
            ),
          ),
        ),
      );
}
