import 'package:flutter/material.dart';
import 'package:yahshua_express_app/home/user_home.dart';

class DeliveredSuccess extends StatefulWidget {
  static String route = 'delivered-success';
  const DeliveredSuccess({Key? key}) : super(key: key);

  @override
  _DeliveredSuccessState createState() => _DeliveredSuccessState();
}

class _DeliveredSuccessState extends State<DeliveredSuccess> {
  //handle device back button
  Future<bool> _onBackPressed() async {
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            actions: <Widget>[
              TextButton(
                onPressed: (){
				Navigator.pushAndRemoveUntil(
                  context,
                   MaterialPageRoute(builder: (context) => const UserScreen()),
                   (Route<dynamic> route) => false,
                );
				}, 
				//=> Navigator.pushNamed(context, UserScreen.route),
                child: const Text('Back to Home',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.green,
                    )),
              ),
              
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text('Cancel',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.red,
                    )),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        body: Center(
          child: SingleChildScrollView(
            // controller: controller,
            child: Column(
              children: const <Widget>[
                Text('AWESOME JOB!'),
                SizedBox(
                  height: 10,
                ),
                Icon(
                  Icons.check_circle,
                  size: 250,
                  color: Colors.amber,
                ),
                SizedBox(
                  height: 10,
                ),
                Text('You have Successfully delivered the Item!'),
                SizedBox(
                  height: 20,
                ),
                Text('"I can do all things through Him who streghthen me."'),
                Text('Philippians 4:13'),
              ],
            ),
          ),
        ),
      ),
    );
  }
  // => Scaffold(
  //       body: Center(
  //         child: SingleChildScrollView(
  //           // controller: controller,
  //           child: Column(
  //             children: const <Widget>[
  //               Text('AWESOME JOB!'),
  //               SizedBox(
  //                 height: 10,
  //               ),
  //               Icon(
  //                 Icons.check_circle,
  //                 size: 250,
  //                 color: Colors.amber,
  //               ),
  //               SizedBox(
  //                 height: 10,
  //               ),
  //               Text('You have Successfully delivered the Item!'),
  //               SizedBox(
  //                 height: 20,
  //               ),
  //               Text('"I can do all things through Him who streghthen me."'),
  //               Text('Philippians 4:13'),
  //             ],
  //           ),
  //         ),
  //       ),
  //     );
}
