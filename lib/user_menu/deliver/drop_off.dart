import 'package:flutter/material.dart';
import 'package:yahshua_express_app/user_menu/deliver/confirm_drop_off_delivered.dart';

class DropOff extends StatefulWidget {
  static String route = 'drop-off';
  const DropOff({
    Key? key,
    required this.requester,
    required this.activity,
    required this.datetime,
    required this.runner,
    required this.item,
    required this.location,
  }) : super(key: key);
  final String activity, datetime, runner, item, location, requester;
  @override
  State<DropOff> createState() => _DropOffState();
}

class _DropOffState extends State<DropOff> {
  List<String> item = ['More', 'Item1', 'Item2'];
  String? itemItem = '';
  @override
  void initState() {
    itemItem = item[0];

    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Drop-off'),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 150,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 100,
                        child: Card(
                          child: Row(
                            children: [
                              Text(widget.requester + '_ Receiver'),
                              const Spacer(),
                              const Icon(Icons.dialer_sip),
                            ],
                          ),
                        ),
                      ),
                      const Text('Drop-off at _:_'),
                    ],
                  ),
                ),
                SizedBox(
                  height: 350,
                  child: Card(
                    shape: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    margin: const EdgeInsets.all(20),
                    shadowColor: Colors.blue,
                    elevation: 10,
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('Item Details'),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(widget.item),
                        Text(widget.location),
                        Text(widget.item + '_ Items'),
                        // const Spacer(),
                        // const Text('More'),
                        // const Icon(Icons.arrow_drop_down),
                        const SizedBox(
                          height: 15,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 150,
                            right: 100,
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                                value: itemItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: item.map(moreMenuItem).toList(),
                                onChanged: (String? value) => setState(
                                      () {
                                        if (value != null) itemItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const Divider(
                          color: Colors.grey,
                        ),
                        const SizedBox(
                          height: 35,
                        ),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => ConfirmDropOffDelivered(
                                    requester: widget.requester,
                                    activity: widget.activity,
                                    datetime: widget.datetime,
                                    runner: widget.runner,
                                    item: widget.item,
                                    location: widget.location)));
                          },
                          child: const Text('Drop-OFF'),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}

DropdownMenuItem<String> moreMenuItem(String item) => DropdownMenuItem(
      value: item,
      child: Text(
        item,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 10,
        ),
      ),
    );
