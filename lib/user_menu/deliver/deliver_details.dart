import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:yahshua_express_app/models/create_user_model.dart';
import 'package:yahshua_express_app/user_menu/deliver/delivery_onway.dart';

class DeliveryDetails extends StatefulWidget {
  static String route = 'delivery-details';
  const DeliveryDetails({
    Key? key,
    required this.requester,
    required this.activity,
    required this.datetime,
    required this.runner,
    required this.item,
    required this.location,
  }) : super(key: key);

  final String activity, datetime, runner, item, location, requester;

  @override
  State<DeliveryDetails> createState() => _DeliveryDetailsState();
}

class _DeliveryDetailsState extends State<DeliveryDetails> {
  //connect data from firebase
  User? user = FirebaseAuth.instance.currentUser;
  CreateUserAccount loggedInUser = CreateUserAccount();
  @override
  void initState() {
    // TODOimplement initState
    super.initState();
    FirebaseFirestore.instance
        .collection("users")
        .doc(user!.uid)
        .get()
        .then((value) {
      // ignore: unnecessary_this
      this.loggedInUser = CreateUserAccount.fromMap(value.data());
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Delivery'),
          centerTitle: true,
        ),
    body: Center(
      child: SingleChildScrollView(
        child: Container(
          // height: double.maxFinite,
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(opacity),
            borderRadius: const BorderRadius.all(
              Radius.circular(20),
            ),
            border: Border.all(
              width: 1.5,
              color: Colors.white.withOpacity(0.7),
            ),
          ),
          child: Column(
            children: [
              SizedBox(
                height: 410,
                child: Card(
                  shape: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  margin: const EdgeInsets.all(20),
                  shadowColor: Colors.blue,
                  elevation: 10,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 10,
                      ),
                      Text(widget.activity),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(children: <Widget>[
                        const Text('A Blessed Day. '),
                        Text(
                          "${loggedInUser.name}!",
                          style: const TextStyle(fontSize: 10),
                        ),
                      ]),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                          'You have booked request with the following details: '),
                      const SizedBox(
                        height: 10,
                      ),
                      Text('Requester: ${widget.requester}'),
                      Text('Item: ${widget.item}'),
                      Text('Date and Time: ${widget.datetime}'),
                      Text('Location: ${widget.location}'),
                      Text('Runner: ${widget.runner}'),
                      const SizedBox(
                        height: 30,
                      ),
                      const Divider(
                        color: Colors.grey,
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      const Text('Do you want to Deliver?'),
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  Navigator.of(context).pop();
                                });
                              },
                              child: const Text('No'),
                            ),
                          ),
                          const Spacer(),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => DeliverOnWay(
                                        requester: widget.requester,
                                        activity: widget.activity,
                                        datetime: widget.datetime,
                                        runner: widget.runner,
                                        item: widget.item,
                                        location: widget.location)));
                              },
                              child: const Text('Yes'),
                            ),
                            const Text(
                                'You have a delivery request from "_" with the following details'),
                            const SizedBox(
                              height: 10,
                            ),
                            Text('Requester: ${widget.requester}'),
                            Text('Item: ${widget.item}'),
                            Text('Date and Time: ${widget.datetime}'),
                            Text('Location: ${widget.location}'),
                            Text('Runner: ${widget.runner}'),
                            const SizedBox(
                              height: 30,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(28.0),
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.amber.withOpacity(opacity),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  border: Border.all(
                                    width: 1.5,
                                    color: Colors.white.withOpacity(0.7),
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    const Text('Do you want to Deliver?'),
                                    Padding(
                                      padding: const EdgeInsets.all(18.0),
                                      child: Row(
                                        children: <Widget>[
                                          ElevatedButton(
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: const Text('No'),
                                          ),
                                          const Spacer(),
                                          ElevatedButton(
                                            onPressed: () {
                                              postDetailsToFirestore();
                                            },
                                            child: const Text('Yes'),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    ),
  );

  postDetailsToFirestore() async {
    // calling our firestore
    // calling our user model
    // sending thes
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;

    DeliveryModel deliveryModel = DeliveryModel();

    // writing all the values
    // userModel.uid = user!.uid;
    deliveryModel.deliveryActivity = widget.activity;
    deliveryModel.deliveryDateTime = widget.datetime;
    deliveryModel.deliverytItem = widget.item;
    deliveryModel.deliveryLocation = widget.location;
    deliveryModel.deliveryRunner = widget.runner;
    deliveryModel.requester = widget.requester;
    deliveryModel.deliveryReleaser = loggedInUser.name;

    await firebaseFirestore
        .collection("delivery")
    // .doc(user.uid)
        .add(deliveryModel.toMap());
    Fluttertoast.showToast(msg: "Saved!");

    // Navigator.pushAndRemoveUntil(
    //     (context),
    //     MaterialPageRoute(builder: (context) => const RequestSuccess()),
    //     (route) => false);
  }
}