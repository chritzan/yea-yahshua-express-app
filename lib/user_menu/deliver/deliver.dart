import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:yahshua_express_app/user_menu/deliver/deliver_details.dart';

class DeliveryScreen extends StatefulWidget {
  static String route = 'Delivery';
  const DeliveryScreen({Key? key}) : super(key: key);

  @override
  _DeliveryScreenState createState() => _DeliveryScreenState();
}

class _DeliveryScreenState extends State<DeliveryScreen> {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.grey,
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Delivery'),
          centerTitle: true,
        ),
        body: Center(
            child: SingleChildScrollView(
          child: Column(
            children: [
              const Text(
                'Items to be Delivered',
                style: TextStyle(fontSize: 20),
              ),
              SizedBox(
                height: 500,
                width: 400,
                child: StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection('request')
                      .where('requestActivity', isEqualTo: 'Delivery')
                      .snapshots(),
                  builder: (context,
                      AsyncSnapshot<QuerySnapshot> streamSnapshot) {
                    if (streamSnapshot.hasData) {
                      return ListView.builder(
                        itemCount: streamSnapshot.data!.docs.length,
                        itemBuilder: (context, index) {
                          final DocumentSnapshot documentSnapshot =
                              streamSnapshot.data!.docs[index];
                          return Card(
                            margin: const EdgeInsets.all(10),
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  Navigator.of(context)
                                      .push(MaterialPageRoute(
                                      builder: (context) => DeliveryDetails(
                                        activity: documentSnapshot[
                                        'requestActivity'],
                                        datetime: documentSnapshot[
                                        'requestDateTime'],
                                        runner: documentSnapshot[
                                        'requestRunner'],
                                        item: documentSnapshot[
                                        'requestItem'],
                                        location: documentSnapshot[
                                        'requestLocation'],
                                        requester: 'N/A',
                                      )));
                                });
                              },
                              child: ListTile(
                                title: Center(
                                    child: Text(documentSnapshot[
                                    'requestActivity'])),
                                subtitle: Center(
                                  child: Column(
                                    children: [
                                      const Text(
                                        'Blessed Day, Thomas Runner! ',
                                      ),
                                      const Text(
                                          'You have pick-up items with the following details: '),
                                      const Text('Requester: N/A'),
                                      Text(documentSnapshot['requestDateTime']),
                                      Text(documentSnapshot['requestRunner']),
                                      Text(documentSnapshot['requestItem']),
                                      Text(documentSnapshot['requestLocation']),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        },
                      );
                    }

                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                ),
              ),
            ],
          ),
        )),
      );
}
