import 'package:flutter/material.dart';
import 'package:yahshua_express_app/user_menu/deliver/goto_drop_off.dart';

class DeliverOnWay extends StatefulWidget {
  static String route = 'delivery-onway';
  const DeliverOnWay({
    Key? key,
    required this.requester,
    required this.activity,
    required this.datetime,
    required this.runner,
    required this.item,
    required this.location,
  }) : super(key: key);
  final String activity, datetime, runner, item, location, requester;

  @override
  State<DeliverOnWay> createState() => _DeliverOnWayState();
}

class _DeliverOnWayState extends State<DeliverOnWay> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Delivery'),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 100,
                  child: Text('For Map vIew'),
                ),
                SizedBox(
                  child: Card(
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.home),
                        ),
                        const Text('Pickup'),
                        const Spacer(),
                        const Text('_min')
                      ],
                    ),
                  ),
                ),
                Text(widget.location),
                Text(widget.runner),
                const Divider(
                  color: Colors.grey,
                ),
                SizedBox(
                  child: Card(
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.location_on),
                        ),
                        const Text('Drop-off'),
                        const Spacer(),
                        const Text('_min')
                      ],
                    ),
                  ),
                ),
                Text(widget.location),
                Text('Receiver: ' + widget.runner),
                Text('Releaser: ' + widget.runner),
                const Divider(
                  color: Colors.grey,
                ),
                const SizedBox(
                  height: 15,
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => GotoDropOff(
                              requester: widget.requester,
                              activity: widget.activity,
                              datetime: widget.datetime,
                              runner: widget.runner,
                              item: widget.item,
                              location: widget.location)));
                    },
                    child: const Text('DELIVER'))
              ],
            ),
          ),
        ),
      );
}
