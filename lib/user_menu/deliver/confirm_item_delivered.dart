import 'package:flutter/material.dart';

class ConnfirmItemDelivered extends StatefulWidget {
  static String route = 'drop-off';
  const ConnfirmItemDelivered({Key? key}) : super(key: key);

  @override
  State<ConnfirmItemDelivered> createState() => _ConnfirmItemDeliveredState();
}

class _ConnfirmItemDeliveredState extends State<ConnfirmItemDelivered> {
  List<String> item = ['More', 'Item1', 'Item2'];
  String? itemItem = '';
  @override
  void initState() {
    itemItem = item[0];

    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Column(
            children: const [
              Text('Confirm Item'),
              Text(
                'Hello "_" Receiver',
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
            ],
          ),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 50,
                  child: Column(
                    children: [
                      Row(
                        children: const [
                          Text('Driver'),
                          Spacer(),
                          Icon(Icons.dialer_sip),
                        ],
                      ),
                      const Text('Drop-off at _:_'),
                    ],
                  ),
                ),
                SizedBox(
                  height: 250,
                  child: Card(
                    shape: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    margin: const EdgeInsets.all(20),
                    shadowColor: Colors.blue,
                    elevation: 10,
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('Item Details'),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('"1234" Transmittal'),
                        const Text('Kauswagan, Cagayan De Oro City'),
                        Row(
                          children: [
                            const Text('_ Items'),
                            const Spacer(),
                            // const Text('More'),
                            // const Icon(Icons.arrow_drop_down),
                            DropdownButtonHideUnderline(
                              child: DropdownButton(
                                  value: itemItem,
                                  iconSize: 36,
                                  icon: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black,
                                  ),
                                  isExpanded: true,
                                  items: item.map(moreMenuItem).toList(),
                                  onChanged: (String? value) => setState(
                                        () {
                                          if (value != null) itemItem = value;
                                        },
                                      )),
                            ),
                          ],
                        ),
                        const Divider(
                          color: Colors.grey,
                        ),
                        const SizedBox(
                          height: 25,
                        ),
                        ElevatedButton(
                          onPressed: () {},
                          child: const Text('CONFIRM'),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}

DropdownMenuItem<String> moreMenuItem(String item) => DropdownMenuItem(
      value: item,
      child: Text(
        item,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 10,
        ),
      ),
    );
