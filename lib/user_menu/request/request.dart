// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:yahshua_express_app/models/request_model.dart';

import 'package:yahshua_express_app/user_menu/request/request_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class RequestPage extends StatefulWidget {
  static String route = 'request';
  const RequestPage({Key? key}) : super(key: key);

  @override
  _RequestPageState createState() => _RequestPageState();
}

//API Key
// android: AIzaSyAee5moJetj9py6KZnINmmBnGPViNKo098
// API 2: AIzaSyBuJ2Gd2EbkzdmXNLLHIR8SVk4yFsuKrAg
// from sir Josh : AIzaSyDUz6kofUOJj5TtLw5dWw0jU6eOQR0thLY
class _RequestPageState extends State<RequestPage> {
  final double opacity = 0.2;
  final TextEditingController dateTimeController = TextEditingController();
  final TextEditingController locationController = TextEditingController();
  final TextEditingController runnerController = TextEditingController();
  final TextEditingController activityController = TextEditingController();
  final TextEditingController itemController = TextEditingController();

  final List<RequestModel> userAccountsLists = [];

  @override
  void initState() {
    super.initState();
  }

  var activityMake, runnerMake, itemMake;
  var setDefaultMake = true, setDefaultMakeModel = true;

  List<String> selected = [];
  List<String> selectedItem = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFFFB74D),
        title: const Text('Book Request'),
        centerTitle: true,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.only(
                  left: 8,
                  right: 8,
                ),
                child: Text(
                  'Note: Please Fill out all the Dropdown and other field before clicking Confirm Button to avoid error!',
                  style: TextStyle(color: Colors.red),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 50,
                child: Center(
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('services')
                        .orderBy('activity')
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      // Safety check to ensure that snapshot contains data
                      // without this safety check, StreamBuilder dirty state warnings will be thrown
                      if (!snapshot.hasData) return Container();

                      return Padding(
                        padding: const EdgeInsets.only(
                          left: 8,
                          right: 8,
                        ),
                        child: Container(
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              hint: const Text('Select Activity'),
                              isExpanded: false,
                              value: activityMake,
                              items: snapshot.data!.docs.map((value) {
                                return DropdownMenuItem(
                                  value: value.get('activity'),
                                  child: Text('${value.get('activity')}'),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(
                                  () {
                                    activityMake = value;
                                  },
                                );
                              },
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.amber.shade100,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: DateTimePicker(
                    controller: dateTimeController,
                    type: DateTimePickerType.dateTimeSeparate,
                    dateMask: 'dd MMM, yyyy',
                    // initialValue: DateTime.now().toString(),
                    firstDate: DateTime(2000),
                    lastDate: DateTime(2100),
                    icon: const Icon(Icons.event),
                    dateLabelText: 'Date',
                    timeLabelText: "Time",
                    selectableDayPredicate: (date) {
                      // Disable weekend days to select from the calendar
                      if (date.weekday == 6 || date.weekday == 7) {
                        return false;
                      }

                      return true;
                    },
                    // ignore: avoid_print
                    onChanged: (val) {
                      dateTimeController.text = val;
                    },
                    // dateTimeController = val,
                    validator: (val) {
                      dateTimeController.text = val!;
                    },
                    // ignore: avoid_print
                    onSaved: (val) {
                      dateTimeController.text = val!;
                    },
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 50,
                child: Center(
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('users')
                        .orderBy('name')
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      // Safety check to ensure that snapshot contains data
                      // without this safety check, StreamBuilder dirty state warnings will be thrown
                      if (!snapshot.hasData) return Container();

                      return Padding(
                        padding: const EdgeInsets.only(
                          left: 8,
                          right: 8,
                        ),
                        child: Container(
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              hint: const Text('Select Runner'),
                              isExpanded: false,
                              value: runnerMake,
                              items: snapshot.data!.docs.map((value) {
                                return DropdownMenuItem(
                                  value: value.get('name'),
                                  child: Text('${value.get('name')}'),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(
                                  () {
                                    runnerMake = value;
                                    // ignore: avoid_print
                                    print(runnerMake);
                                  },
                                );
                              },
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 50,
                child: Center(
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('items')
                        .orderBy('name')
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      // Safety check to ensure that snapshot contains data
                      // without this safety check, StreamBuilder dirty state warnings will be thrown
                      if (!snapshot.hasData) return Container();

                      return Padding(
                        padding: const EdgeInsets.only(
                          left: 8,
                          right: 8,
                        ),
                        child: Container(
                          width: double.maxFinite,
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              hint: const Text('Select Item'),
                              isExpanded: false,
                              value: itemMake,
                              items: snapshot.data!.docs.map((value) {
                                return DropdownMenuItem(
                                  value: value.get('name'),
                                  child: Text('${value.get('name')}'),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(
                                  () {
                                    itemMake = value;
                                    // ignore: avoid_print
                                    print(itemMake);
                                  },
                                );
                              },
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: locationController,
                  textInputAction: TextInputAction.done,
                  decoration: InputDecoration(
                    fillColor: Colors.lightBlue,
                    contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
                    labelText: "Enter Location",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  if (activityMake == null &&
                      runnerMake == null &&
                      itemMake == null) {
                    showSnackBar("All Field are Required!",
                        const Duration(milliseconds: 1000));
                  } else {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => RequestData(
                            activity: activityMake,
                            datetime: dateTimeController.text,
                            runner: runnerMake,
                            item: itemMake,
                            location: locationController.text)));
                  }
                },
                // save,
                child: const Text('Confirm'),
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ),
      ),
    );
  }

  showSnackBar(String snackText, Duration d) {
    final snackBar = SnackBar(content: Text(snackText), duration: d);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
