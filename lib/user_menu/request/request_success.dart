import 'package:flutter/material.dart';

class RequestSuccess extends StatefulWidget {
  static String route = 'request-success';
  const RequestSuccess({Key? key}) : super(key: key);

  @override
  _RequestSuccessState createState() => _RequestSuccessState();
}

class _RequestSuccessState extends State<RequestSuccess> {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: SingleChildScrollView(
            // controller: controller,
            child: Column(
              children: const <Widget>[
                Text('Request Booked!'),
                SizedBox(
                  height: 10,
                ),
                Icon(
                  Icons.check_circle,
                  size: 250,
                  color: Colors.amber,
                ),
                SizedBox(
                  height: 10,
                ),
                Text('The request was Successfully booked.'),
                SizedBox(
                  height: 20,
                ),
                Text('"I can Do all things through Him who streghthen me."'),
                Text('Philippians 4:13'),
              ],
            ),
          ),
        ),
      );
}
