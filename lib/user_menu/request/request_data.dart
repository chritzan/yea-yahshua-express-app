import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:yahshua_express_app/models/request_model.dart';
import 'package:yahshua_express_app/models/create_user_model.dart';
import 'package:yahshua_express_app/user_menu/request/request_success.dart';

class RequestData extends StatefulWidget {
  static String route = 'request-information';
  RequestData({
    Key? key,
    required this.activity,
    required this.datetime,
    required this.runner,
    required this.item,
    required this.location,
  }) : super(key: key);
  final List requestData = ['1', '2', '3', '4', '5'];
  final String activity, datetime, runner, item, location;

  @override
  State<RequestData> createState() => _RequestDataState();
}

class _RequestDataState extends State<RequestData> {
  final double opacity = 0.2;

  //connect data from firebase
  User? user = FirebaseAuth.instance.currentUser;
  CreateUserAccount loggedInUser = CreateUserAccount();
  @override
  void initState() {
    // TODOimplement initState
    super.initState();
    FirebaseFirestore.instance
        .collection("users")
        .doc(user!.uid)
        .get()
        .then((value) {
      // ignore: unnecessary_this
      this.loggedInUser = CreateUserAccount.fromMap(value.data());
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: const Color(0xFFFFB74D),
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Book Request'),
          centerTitle: true,
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              // height: double.maxFinite,
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(opacity),
                borderRadius: const BorderRadius.all(
                  Radius.circular(20),
                ),
                border: Border.all(
                  width: 1.5,
                  color: Colors.white.withOpacity(0.7),
                ),
              ),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SizedBox(
                      height: 380,
                      child: SingleChildScrollView(
                        child: Card(
                          child: Center(
                            child: Column(
                              children: [
                                Text(widget.activity),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(children: <Widget>[
                                  const Text('A Blessed Day. '),
                                  Text(
                                    "${loggedInUser.name}!",
                                    style: const TextStyle(fontSize: 10),
                                  ),
                                ]),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Text(
                                    'You have booked request with the following details: '),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text('Item: ${widget.item}'),
                                Text('Date and Time: ${widget.datetime}'),
                                Text('Location: ${widget.location}'),
                                Text('Runner: ${widget.runner}'),
                                const SizedBox(
                                  height: 30,
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(28.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.amber.withOpacity(opacity),
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      border: Border.all(
                                        width: 1.5,
                                        color: Colors.white.withOpacity(0.7),
                                      ),
                                    ),
                                    child: Column(
                                      children: [
                                        const Text('Do you want to proceed?'),
                                        Padding(
                                          padding: const EdgeInsets.all(18.0),
                                          child: Row(
                                            children: <Widget>[
                                              ElevatedButton(
                                                onPressed: () {
                                                  Navigator.of(context).pop();
                                                },
                                                child: const Text('No'),
                                              ),
                                              const Spacer(),
                                              ElevatedButton(
                                                onPressed: () {
                                                  postDetailsToFirestore();
                                                },
                                                child: const Text('Yes'),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );

  postDetailsToFirestore() async {
    // calling our firestore
    // calling our user model
    // sending thes
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;

    RequestModel requestModel = RequestModel();

    // writing all the values
    // userModel.uid = user!.uid;
    requestModel.requestActivity = widget.activity; 
    requestModel.requestDateTime = widget.datetime;
    requestModel.requestItem = widget.item;
    requestModel.requestLocation = widget.location;
    requestModel.requestRunner = widget.runner;
    requestModel.requester = loggedInUser.name;

    await firebaseFirestore.collection("request").add(requestModel.toMap());
    Fluttertoast.showToast(msg: "Successfully Requested!");

    Navigator.pushAndRemoveUntil(
        (context),
        MaterialPageRoute(builder: (context) => const RequestSuccess()),
        (route) => false);
  }
}
