import 'package:flutter/material.dart';
import 'package:yahshua_express_app/user_menu/pick_up/update_requester.dart';

class UpdateItem extends StatefulWidget {
  const UpdateItem({Key? key}) : super(key: key);

  @override
  _UpdateItemState createState() => _UpdateItemState();
}

class _UpdateItemState extends State<UpdateItem> {
  int _value = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFFFFB74D),
        title: const Text('Update Activity'),
        centerTitle: true,
      ),
      body: Center(
        child: SingleChildScrollView(
          child: SizedBox(
            height: double.maxFinite,
            child: Column(children: [
              const SizedBox(
                height: 100,
              ),
              SizedBox(
                height: 70,
                child: Card(
                  child: Column(children: [
                    const Text('Is the Request Succesfully Picked-up?'),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => setState(() => _value = 0),
                          child: Container(
                            height: 30,
                            width: 56,
                            color: _value == 0
                                ? Colors.yellow
                                : Colors.transparent,
                            child: const Center(child: Text('No')),
                          ),
                        ),
                        const SizedBox(width: 4),
                        GestureDetector(
                          onTap: () => setState(() => _value = 1),
                          child: Container(
                            height: 30,
                            width: 56,
                            color: _value == 1
                                ? Colors.yellow
                                : Colors.transparent,
                            child: const Center(child: Text('Yes')),
                          ),
                        ),
                      ],
                    ),
                  ]),
                ),
              ),
              Row(
                children: [
                  const Spacer(),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const UpdateRequester(),
                      ));
                    },
                    child: const Text('Next'),
                  ),
                ],
              )
            ]),
          ),
        ),
      ),
    );
  }
}
