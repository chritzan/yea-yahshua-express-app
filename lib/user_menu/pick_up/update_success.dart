import 'package:flutter/material.dart';

class UpdateSuccess extends StatefulWidget {
  static String route = 'request-success';
  const UpdateSuccess({Key? key}) : super(key: key);

  @override
  _UpdateSuccessState createState() => _UpdateSuccessState();
}

class _UpdateSuccessState extends State<UpdateSuccess> {
  @override
  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: SingleChildScrollView(
        // controller: controller,
        child: Column(
          children: const <Widget>[
            Text('Awesome Job!'),
            SizedBox(
              height: 10,
            ),
            Icon(
              Icons.check_circle,
              size: 250,
              color: Colors.amber,
            ),
            SizedBox(
              height: 10,
            ),
            Text('You have successfully picked-up the item'),
            SizedBox(
              height: 20,
            ),
            Text('"I can Do all things through Him who streghthen me."'),
            Text('Philippians 4:13'),
          ],
        ),
      ),
    ),
  );
}
