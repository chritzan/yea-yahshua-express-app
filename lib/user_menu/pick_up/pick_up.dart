import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:yahshua_express_app/user_menu/pick_up/pick_up_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PickUp extends StatefulWidget {
  static String route = 'pick-up';
  const PickUp({Key? key}) : super(key: key);

  @override
  _PickUpState createState() => _PickUpState();
}

class _PickUpState extends State<PickUp> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Pick-Up'),
          centerTitle: true,
          actions: const [
            Icon(Icons.arrow_back),
          ],
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              height: double.maxFinite,
              width: double.maxFinite,
              color: const Color(0xFFFFF5A3),
              child: Column(
                children: [
                  const Text("Items to Pick-Up"),
                  const SizedBox(height: 30),
                  SizedBox(
                    height: 500,
                    width: 400,
                    child: StreamBuilder(
                      stream: FirebaseFirestore.instance
                          .collection('request')
                          .where('requestActivity', isEqualTo: 'Pick-up')
                          .snapshots(),
                      builder: (context,
                          AsyncSnapshot<QuerySnapshot> streamSnapshot) {
                        if (streamSnapshot.hasData) {
                          return ListView.builder(
                            itemCount: streamSnapshot.data!.docs.length,
                            itemBuilder: (context, index) {
                              final DocumentSnapshot documentSnapshot =
                                  streamSnapshot.data!.docs[index];
                              return Card(
                                margin: const EdgeInsets.all(10),
                                child: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      Navigator.of(context)
                                          .push(MaterialPageRoute(
                                              builder: (context) => PickUpData(
                                                    activity: documentSnapshot[
                                                        'requestActivity'],
                                                    datetime: documentSnapshot[
                                                        'requestDateTime'],
                                                    runner: documentSnapshot[
                                                        'requestRunner'],
                                                    item: documentSnapshot[
                                                        'requestItem'],
                                                    location: documentSnapshot[
                                                        'requestLocation'],
                                                    requester: 'N/A',
                                                  )));
                                    });
                                  },
                                  child: ListTile(
                                    title: Center(
                                        child: Text(documentSnapshot[
                                            'requestActivity'])),
                                    subtitle: Center(
                                      child: Column(
                                        children: [
                                          const Text(
                                            'Blessed Day, Thomas Runner! ',
                                          ),
                                          const Text(
                                              'You have pick-up items with the following details: '),
                                          const Text('Requester: N/A'),
                                          Text(documentSnapshot[
                                              'requestDateTime']),
                                          Text(documentSnapshot[
                                              'requestRunner']),
                                          Text(documentSnapshot['requestItem']),
                                          Text(documentSnapshot[
                                              'requestLocation']),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        }

                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
