import 'package:flutter/material.dart';
import 'package:yahshua_express_app/user_menu/pick_up/update_success.dart';

class UpdateRequester extends StatefulWidget {
  const UpdateRequester({Key? key}) : super(key: key);

  @override
  _UpdateRequesterState createState() => _UpdateRequesterState();
}

class _UpdateRequesterState extends State<UpdateRequester> {

  @override
  Widget build(BuildContext context) => Scaffold(
    appBar: AppBar(
      backgroundColor: const Color(0xFFFFB74D),
      title: const Text('Update Activity'),
      centerTitle: true,
      actions: const [
        Icon(Icons.arrow_back),
      ],
    ),
    body: Center(
      child: SingleChildScrollView(
        child: SizedBox(
          height: double.maxFinite,
          child: Column(
            children: [
              const SizedBox(
                height: 100,
              ),
              SizedBox(
                height: 100,
                child: Card(
                  child: Column(
                    children: [
                      const Text('Do you want to notify the requester?'),
                      Row(

                        children: [
                          ElevatedButton(
                            onPressed: () {},
                            child: const Text('Proceed'),
                          )
                        ],
                      ),

                    ],
                  ),
                ),
              ),

              Row(
                children: [
                  const Spacer(),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(
                        builder: (context) =>
                        const UpdateSuccess(),
                      ));
                    },
                    child: const Text('Next'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    ),
  );
}
