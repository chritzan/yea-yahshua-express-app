import 'dart:io';

import 'package:camera/camera.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:yahshua_express_app/models/pick_up_model.dart';

import 'package:yahshua_express_app/user_menu/pick_up/update_success.dart';

import 'package:firebase_auth/firebase_auth.dart';

import 'package:yahshua_express_app/models/create_user_model.dart';

class ConfirmPickUp extends StatefulWidget {
  const ConfirmPickUp({
    Key? key,
    required this.requester,
    required this.activity,
    required this.datetime,
    required this.runner,
    required this.item,
    required this.location,
  }) : super(key: key);
  final String activity, datetime, runner, item, location, requester;

  @override
  _ConfirmPickUpState createState() => _ConfirmPickUpState();
}

class _ConfirmPickUpState extends State<ConfirmPickUp> {
  List<CameraDescription>? cameras; //list out the camera available
  CameraController? controller; //controller for camera
  XFile? image; //for caputred image
  String? downloadURL;
  UploadTask? uploadTask;

  final double opacity = 0.2;

  @override
  void initState() {
    loadCamera();

    super.initState();
    FirebaseFirestore.instance
        .collection("users")
        .doc(user!.uid)
        .get()
        .then((value) {
      // ignore: unnecessary_this
      this.loggedInUser = CreateUserAccount.fromMap(value.data());
      setState(() {});
    });
  }

  //connect data from firebase
  User? user = FirebaseAuth.instance.currentUser;
  CreateUserAccount loggedInUser = CreateUserAccount();

  loadCamera() async {
    cameras = await availableCameras();
    if (cameras != null) {
      controller = CameraController(cameras![0], ResolutionPreset.max);
      //cameras[0] = first camera, change to 1 to another camera

      controller!.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    } else {
      // ignore: avoid_print
      print("NO any camera found");
    }
  }

  Future uploadFile() async {
    final path = 'pickup/${image!.name}';
    final file = File(image!.path);

    final ref = FirebaseStorage.instance.ref().child(path);

    setState(() {
      uploadTask = ref.putFile(file);
      if (file != null) {
        File(image!.path);
      } else {
        showSnackBar("No File selected", const Duration(milliseconds: 400));
      }
    });

    final snapshot = await uploadTask!.whenComplete(() {});

    final urlDownload = await snapshot.ref.getDownloadURL();
    // ignore: avoid_print
    print('Download Link: $urlDownload');

    setState(() {
      uploadTask = null;
    });
  }

  // // uploading the image to firebase cloudstore
  // Future uploadImage(File image) async {
  //   final imgId = DateTime.now().millisecondsSinceEpoch.toString();
  //   FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  //   Reference reference = FirebaseStorage.instance
  //       .ref()
  //       .child('${widget.userId}/images')
  //       .child("post_$imgId");

  //   await reference.putFile(image);
  //   downloadURL = await reference.getDownloadURL();

  //   // cloud firestore
  //   await firebaseFirestore
  //       .collection("pickup")
  //       .doc(widget.userId)
  //       .collection("images")
  //       .add({'downloadURL': downloadURL}).whenComplete(
  //           () => showSnackBar("Image Uploaded", const Duration(seconds: 10)));
  // }

  int _value = 0;
  int _value1 = 0;
  int _updateValue = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: Column(children: const [
            Text('Pick Up'),
            Text(
              'Hello Thomas Runner',
              style: TextStyle(fontSize: 15.0),
            ),
          ]),
          centerTitle: true,
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white.withOpacity(opacity),
                borderRadius: const BorderRadius.all(
                  Radius.circular(20),
                ),
                border: Border.all(
                  width: 1.5,
                  color: Colors.white.withOpacity(0.2),
                ),
                image: const DecorationImage(
                  image: NetworkImage(
                      "https://firebasestorage.googleapis.com/v0/b/flutterbricks-public.appspot.com/o/backgrounds%2Fcasey-horner-G2jAOMGGlPE-unsplash.jpg?alt=media&token=54d2effa-1220-4bc2-b03c-caed9feb22db"),
                  fit: BoxFit.cover,
                ),
                gradient: const RadialGradient(
                  center: Alignment.center,
                  radius: 1,
                  colors: <Color>[Color(0xFF4FC3F7), Color(0xFFB2EBF2)],
                  stops: [0, 1],
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(opacity),
                      borderRadius: const BorderRadius.all(
                        Radius.circular(20),
                      ),
                      border: Border.all(
                        width: 1.5,
                        color: Colors.white.withOpacity(0.2),
                      ),
                      image: const DecorationImage(
                        image: NetworkImage(
                            "https://firebasestorage.googleapis.com/v0/b/flutterbricks-public.appspot.com/o/backgrounds%2Fluke-chesser-3rWagdKBF7U-unsplash.jpg?alt=media&token=9c5fd84c-2d31-4772-91c5-6e2be82797ba"),
                        fit: BoxFit.cover,
                      ),
                      gradient: const RadialGradient(
                        center: Alignment.center,
                        radius: 1,
                        colors: <Color>[Color(0xFF3EF3D7), Color(0xFFB2EBF2)],
                        stops: [0, 1],
                      ),
                    ),
                    margin: const EdgeInsets.all(25),
                    child: Column(
                      children: [
                        ElevatedButton(
                          child: const Text(
                            'Confirm Item',
                            style: TextStyle(fontSize: 20.0),
                          ),
                          onPressed: () {
                            // Navigator.of(context).push(MaterialPageRoute(
                            //   builder: (context) => const ConfirmItem(),
                            // ));
                          },
                        ),
                        SizedBox(
                          height: 60,
                          child: Card(
                            child: Column(children: [
                              const Text('Is the Item available?'),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () => setState(() => _value = 0),
                                    child: Container(
                                      height: 30,
                                      width: 56,
                                      color: _value == 0
                                          ? Colors.yellow
                                          : Colors.transparent,
                                      child: const Center(child: Text('No')),
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  GestureDetector(
                                    onTap: () => setState(() => _value = 1),
                                    child: Container(
                                      height: 30,
                                      width: 56,
                                      color: _value == 1
                                          ? Colors.yellow
                                          : Colors.transparent,
                                      child: const Center(child: Text('Yes')),
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                          ),
                        ),
                        SizedBox(
                          height: 60,
                          child: Card(
                            child: Column(children: [
                              const Text('Is the releaser available?'),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () => setState(() => _value1 = 0),
                                    child: Container(
                                      height: 30,
                                      width: 56,
                                      color: _value1 == 0
                                          ? Colors.yellow
                                          : Colors.transparent,
                                      child: const Center(child: Text('No')),
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  GestureDetector(
                                    onTap: () => setState(() => _value1 = 1),
                                    child: Container(
                                      height: 30,
                                      width: 56,
                                      color: _value1 == 1
                                          ? Colors.yellow
                                          : Colors.transparent,
                                      child: const Center(child: Text('Yes')),
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                          ),
                        ),
                        ElevatedButton(
                          onPressed: () {},
                          child: const Text('Contact Requester'),
                        ),
                        ElevatedButton(
                          onPressed: () {},
                          child: const Text('Contact Releaser'),
                        )
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(opacity),
                      borderRadius: const BorderRadius.all(
                        Radius.circular(20),
                      ),
                      border: Border.all(
                        width: 1.5,
                        color: Colors.white.withOpacity(0.2),
                      ),
                      image: const DecorationImage(
                        image: NetworkImage(
                            "https://firebasestorage.googleapis.com/v0/b/flutterbricks-public.appspot.com/o/backgrounds%2Fluke-chesser-3rWagdKBF7U-unsplash.jpg?alt=media&token=9c5fd84c-2d31-4772-91c5-6e2be82797ba"),
                        fit: BoxFit.cover,
                      ),
                      gradient: const RadialGradient(
                        center: Alignment.center,
                        radius: 1,
                        colors: <Color>[Color(0xFF4FC3F7), Color(0xFFB2EBF2)],
                        stops: [0, 1],
                      ),
                    ),
                    margin: const EdgeInsets.all(25),
                    child: Column(
                      children: [
                        ElevatedButton(
                          child: const Text(
                            'Document Item',
                            style: TextStyle(fontSize: 20.0),
                          ),
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                      content: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: SizedBox(
                                                  height: 300,
                                                  width: 400,
                                                  child: controller == null
                                                      ? const Center(
                                                          child: Text(
                                                              "Loading Camera..."))
                                                      : !controller!.value
                                                              .isInitialized
                                                          ? const Center(
                                                              child:
                                                                  CircularProgressIndicator(),
                                                            )
                                                          : CameraPreview(
                                                              controller!)),
                                            ),
                                            ElevatedButton.icon(
                                                onPressed: () async {
                                                  try {
                                                    if (controller != null) {
                                                      //check if contrller is not null
                                                      if (controller!.value
                                                          .isInitialized) {
                                                        //check if controller is initialized
                                                        image = await controller!
                                                            .takePicture(); //capture image
                                                        setState(() {
                                                          //update UI
                                                        });
                                                      }
                                                    }
                                                  } catch (e) {
                                                    // ignore: avoid_print
                                                    print(e); //show error
                                                  }
                                                  Navigator.of(context).pop();
                                                },
                                                icon: const Icon(Icons.camera),
                                                label: const Text('Capture')),
                                          ],
                                        ),
                                      ),
                                    ));
                          },
                        ),
                        SizedBox(
                          // width: 300,
                          // height: 350,
                          //show captured image
                          // padding: const EdgeInsets.all(30),
                          child: image == null
                              ? const Text("No image captured")
                              : Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.file(
                                    File(image!.path),
                                    // width: 400,
                                    height: 200,
                                  ),
                                ),
                          //display captured image
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        ElevatedButton(
                            onPressed: () {
                              uploadFile();
                            },
                            child: const Text('Upload')),
                        buildProgress(),
                      ],
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(opacity),
                      borderRadius: const BorderRadius.all(
                        Radius.circular(20),
                      ),
                      border: Border.all(
                        width: 1.5,
                        color: Colors.white.withOpacity(0.2),
                      ),
                      image: const DecorationImage(
                        image: NetworkImage(
                            "https://firebasestorage.googleapis.com/v0/b/flutterbricks-public.appspot.com/o/backgrounds%2Fluke-chesser-3rWagdKBF7U-unsplash.jpg?alt=media&token=9c5fd84c-2d31-4772-91c5-6e2be82797ba"),
                        fit: BoxFit.cover,
                      ),
                      gradient: const RadialGradient(
                        center: Alignment.center,
                        radius: 1,
                        colors: <Color>[Color(0xFF4FC3F7), Color(0xFFB2EBF2)],
                        stops: [0, 1],
                      ),
                    ),
                    margin: const EdgeInsets.all(25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          child: const Text(
                            'Update Activity',
                            style: TextStyle(fontSize: 20.0),
                          ),
                          onPressed: () {
                            // Navigator.of(context).push(MaterialPageRoute(
                            //   builder: (context) => const UpdateItem(),
                            // ));
                          },
                        ),
                        SizedBox(
                          height: 70,
                          child: Card(
                            child: Column(children: [
                              const Text(
                                  'Is the Request Succesfully Picked-up?'),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () =>
                                        setState(() => _updateValue = 0),
                                    child: Container(
                                      height: 30,
                                      width: 56,
                                      color: _updateValue == 0
                                          ? Colors.yellow
                                          : Colors.transparent,
                                      child: const Center(child: Text('No')),
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  GestureDetector(
                                    onTap: () =>
                                        setState(() => _updateValue = 1),
                                    child: Container(
                                      height: 30,
                                      width: 56,
                                      color: _updateValue == 1
                                          ? Colors.yellow
                                          : Colors.transparent,
                                      child: const Center(child: Text('Yes')),
                                    ),
                                  ),
                                ],
                              ),
                            ]),
                          ),
                        ),
                        // Row(
                        //   children: [
                        //     const Spacer(),
                        //     ElevatedButton(
                        //       onPressed: () {
                        //         Navigator.of(context).push(MaterialPageRoute(
                        //           builder: (context) => const UpdateRequester(),
                        //         ));
                        //       },
                        //       child: const Text('Next'),
                        //     ),
                        //   ],
                        // ),
                        SizedBox(
                          height: 100,
                          child: Card(
                            child: Column(
                              children: [
                                const Text(
                                    'Do you want to notify the requester?'),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    ElevatedButton(
                                      onPressed: () {},
                                      child: const Text('Proceed'),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // const Spacer(),
                      // const Text('Save Details?'),

                      ElevatedButton(
                        onPressed: () {
                          // Navigator.of(context).push(MaterialPageRoute(
                          //   builder: (context) => const UpdateSuccess(),
                          // ));
                          postDetailsToFirestore();
                          Navigator.pushAndRemoveUntil(
                              (context),
                              MaterialPageRoute(
                                  builder: (context) => const UpdateSuccess()),
                              (route) => false);
                        },
                        child: const Text('Save Details'),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildProgress() => StreamBuilder<TaskSnapshot>(
      stream: uploadTask?.snapshotEvents,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final data = snapshot.data!;
          double progress = data.bytesTransferred / data.totalBytes;

          return SizedBox(
            height: 50,
            child: Stack(
              fit: StackFit.expand,
              children: [
                LinearProgressIndicator(
                  value: progress,
                  backgroundColor: Colors.amber,
                  color: Colors.green,
                ),
                Center(
                  child: Text(
                    '${(100 * progress).roundToDouble()}%',
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
          );
        } else {
          return const SizedBox(
            height: 50,
          );
        }
      });

  showSnackBar(String snackText, Duration d) {
    final snackBar = SnackBar(content: Text(snackText), duration: d);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  postDetailsToFirestore() async {
    // calling our firestore
    // calling our user model
    // sending thes
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;

    PickUpModel pickUpModel = PickUpModel();

    // writing all the values
    // userModel.uid = user!.uid;
    pickUpModel.pickUpActivity = widget.activity;
    pickUpModel.pickUpDateTime = widget.datetime;
    pickUpModel.pickUptItem = widget.item;
    pickUpModel.pickUpLocation = widget.location;
    pickUpModel.pickUpRunner = widget.runner;
    pickUpModel.requester = widget.requester;
    pickUpModel.pickUpReleaser = loggedInUser.name;
    // pickUpModel.itemAvailable = _value;

    await firebaseFirestore
        .collection("pickup")
        // .doc(user.uid)
        .add(pickUpModel.toMap());
    Fluttertoast.showToast(msg: "Saved!");

    // Navigator.pushAndRemoveUntil(
    //     (context),
    //     MaterialPageRoute(builder: (context) => const RequestSuccess()),
    //     (route) => false);
  }
}
