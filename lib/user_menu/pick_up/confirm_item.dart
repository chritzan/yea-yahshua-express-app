import 'package:flutter/material.dart';

class ConfirmItem extends StatefulWidget {
  static String route = 'pick-up';
  const ConfirmItem({Key? key}) : super(key: key);

  @override
  _ConfirmState createState() => _ConfirmState();
}

class _ConfirmState extends State<ConfirmItem> {
  int _value = 0;
  int _value1 = 0;

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Confirm Item'),
          centerTitle: true,
          actions: const [],
        ),
        body: Center(
          child: SingleChildScrollView(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              SizedBox(
                height: 60,
                child: Card(
                  child: Column(children: [
                    const Text('Is the Item available?'),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => setState(() => _value = 0),
                          child: Container(
                            height: 30,
                            width: 56,
                            color: _value == 0
                                ? Colors.yellow
                                : Colors.transparent,
                            child: const Center(child: Text('No')),
                          ),
                        ),
                        const SizedBox(width: 4),
                        GestureDetector(
                          onTap: () => setState(() => _value = 1),
                          child: Container(
                            height: 30,
                            width: 56,
                            color: _value == 1
                                ? Colors.yellow
                                : Colors.transparent,
                            child: const Center(child: Text('Yes')),
                          ),
                        ),
                      ],
                    ),
                  ]),
                ),
              ),
              SizedBox(
                height: 60,
                child: Card(
                  child: Column(children: [
                    const Text('Is the releaser available?'),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => setState(() => _value1 = 0),
                          child: Container(
                            height: 30,
                            width: 56,
                            color: _value1 == 0
                                ? Colors.yellow
                                : Colors.transparent,
                            child: const Center(child: Text('No')),
                          ),
                        ),
                        const SizedBox(width: 4),
                        GestureDetector(
                          onTap: () => setState(() => _value1 = 1),
                          child: Container(
                            height: 30,
                            width: 56,
                            color: _value1 == 1
                                ? Colors.yellow
                                : Colors.transparent,
                            child: const Center(child: Text('Yes')),
                          ),
                        ),
                      ],
                    ),
                  ]),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    onPressed: () {},
                    child: const Text('Contact Requester'),
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    child: const Text('Contact Releaser'),
                  )
                ],
              ),
              Row(
                children: [
                  const Spacer(),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('Next'),
                  ),
                ],
              )
            ]),
          ),
        ),
      );
}
