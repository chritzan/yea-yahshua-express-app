import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

class DocumentItem extends StatefulWidget {
  final String? userId;
  const DocumentItem({Key? key, this.userId}) : super(key: key);

  @override
  State<DocumentItem> createState() => _DocumentItemState();
}

class _DocumentItemState extends State<DocumentItem> {
  File? _image;
  final imagePicker = ImagePicker();
  // initializing some values
  String? downloadURL;

  Future imagePickerMethodCamera() async {
    final pick = await imagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      if (pick != null) {
        _image = File(pick.path);
      } else {
        showSnackBar("No File selected", const Duration(milliseconds: 400));
      }
    });
  }

  // uploading the image to firebase cloudstore
  Future uploadImage(File _image) async {
    final imgId = DateTime.now().millisecondsSinceEpoch.toString();
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    Reference reference = FirebaseStorage.instance
        .ref()
        .child('${widget.userId}/images')
        .child("post_$imgId");

    await reference.putFile(_image);
    downloadURL = await reference.getDownloadURL();

    // cloud firestore
    await firebaseFirestore
        .collection("users")
        .doc(widget.userId)
        .collection("images")
        .add({'downloadURL': downloadURL}).whenComplete(
            () => showSnackBar("Image Uploaded", const Duration(seconds: 10)));
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Document Item'),
          centerTitle: true,
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text('TAKE A PICTURE'),
                    Icon(Icons.camera),
                  ],
                ),
                ElevatedButton(
                  onPressed: () {
                    imagePickerMethodCamera();
                  },
                  child: const Text('Capture Image'),
                ),
                SizedBox(
                  height: 200,
                  // radius: 100,
                  // backgroundColor: const Color.fromRGBO(139, 207, 2, 90),
                  child: Card(
                    child: _image == null
                        ? const Center(child: Text("No image selected"))
                        : Image.file(_image!),
                  ),
                ),
                Row(
                  children: [
                    const Spacer(),
                    ElevatedButton(
                      onPressed: () {
                        if (_image == null) {
                          showSnackBar("Select Image first",
                              const Duration(milliseconds: 400));
                        }
                        uploadImage(_image!);
                        // Navigator.of(context).push(MaterialPageRoute(
                        //   builder: (context) => const DocumentItemSignature(),
                        // ));
                      },
                      child: const Text('Next'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );

  showSnackBar(String snackText, Duration d) {
    final snackBar = SnackBar(content: Text(snackText), duration: d);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
