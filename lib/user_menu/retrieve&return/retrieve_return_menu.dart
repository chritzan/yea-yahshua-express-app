import 'package:flutter/material.dart';
import 'package:yahshua_express_app/user_menu/retrieve&return/retrieve/retrieve.dart';
import 'package:yahshua_express_app/user_menu/retrieve&return/return/return.dart';

class RetrieveReturnMenu extends StatefulWidget {
  const RetrieveReturnMenu({Key? key}) : super(key: key);

  @override
  _RetrieveReturnMenuState createState() => _RetrieveReturnMenuState();
}

class _RetrieveReturnMenuState extends State<RetrieveReturnMenu> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Retrieve and Return'),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const RetrieveItem(),
                    ));
                  },
                  child: const Text('Retrieve')),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const ReturnItem(),
                    ));
                  },
                  child: const Text('Return')),
            ],
          ),
        ),
      );
}
