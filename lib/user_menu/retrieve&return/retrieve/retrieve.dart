import 'package:flutter/material.dart';

class RetrieveItem extends StatefulWidget {
  const RetrieveItem({Key? key}) : super(key: key);

  @override
  _RetrieveItemState createState() => _RetrieveItemState();
}

class _RetrieveItemState extends State<RetrieveItem> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Retrieve'),
          centerTitle: true,
        ),
      );
}
