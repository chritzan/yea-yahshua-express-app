import 'package:flutter/material.dart';

class ReturnItem extends StatefulWidget {
  const ReturnItem({Key? key}) : super(key: key);

  @override
  _ReturnItemState createState() => _ReturnItemState();
}

class _ReturnItemState extends State<ReturnItem> {
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Return'),
          centerTitle: true,
        ),
      );
}
