import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:yahshua_express_app/admin_menu/setting/item_setting.dart';
import 'package:yahshua_express_app/models/items/create_item_model.dart';

class AddItemsWidget extends StatefulWidget {
  const AddItemsWidget({Key? key}) : super(key: key);
  static String route = "add-items";

  @override
  _AddItemsWidgetState createState() => _AddItemsWidgetState();
}

class _AddItemsWidgetState extends State<AddItemsWidget> {
  // bool
  bool isActive = false;

  // string for displaying the error Message
  String? errorMessage;

  // our form key
  final _formKey = GlobalKey<FormState>();

  // editing Controllers
  final nameEditingController = TextEditingController();
  final departmentEditingController = TextEditingController();
  final emailEditingController = TextEditingController();
  final passwordEditingController = TextEditingController();
  final locationEditingController = TextEditingController();
  final positionEditingController = TextEditingController();
  final typeEditingController = TextEditingController();
  bool isLoading = false;

  final double opacity = 0.4;

  @override
  Widget build(BuildContext context) {
    //responsiveness
    final size = MediaQuery.of(context).size;
    //first name field
    final nameField = TextFormField(
      autofocus: false,
      controller: nameEditingController,
      keyboardType: TextInputType.name,
      validator: (value) {
        RegExp regex = RegExp(r'^.{3,}$');
        if (value!.isEmpty) {
          return ("Name is Required");
        }
        if (!regex.hasMatch(value)) {
          return ("Minimum 3 Characters");
        }
        return null;
      },
      onSaved: (value) {
        nameEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        prefixIcon:
            const Icon(Icons.account_circle, color: Colors.lightBlueAccent),
        contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
        labelText: "Name",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );

    //Sign Up Button
    final createButton = Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(30),
      color: Colors.amber.shade200,
      child: MaterialButton(
        padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: () {
          if (nameEditingController.text.isNotEmpty) {
            setState(() {
              isLoading = true;
            });
          }
          postDetailsToFirestore();
        },
        child: const Text(
          "Create",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );

    return MaterialApp(
      //
      theme: ThemeData.light().copyWith(
          //hintColor: Colors.blue,
          ),
      home: Scaffold(
        backgroundColor: Colors.white,
        body: isLoading
            ? Center(
                child: SizedBox(
                  height: size.height / 20,
                  width: size.height / 20,
                  child: const CircularProgressIndicator(
                    color: Colors.lightGreen,
                  ),
                ),
              )
            : Center(
                child: Stack(
                  children: [
                    Container(
                      constraints: const BoxConstraints.expand(),
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(opacity),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20),
                        ),
                        border: Border.all(
                          width: 1.5,
                          color: Colors.white.withOpacity(0.7),
                        ),
                      ),
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            left: 10,
                            right: 10,
                            top: 60,
                          ),
                          child: Form(
                            key: _formKey,
                            child: Container(
                              //color: const Color.fromRGBO(63, 255, 255, 90),
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(opacity),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(20),
                                ),
                                border: Border.all(
                                  width: 1.5,
                                  color: Colors.white.withOpacity(0.7),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(
                                      "Create Item",
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.amber,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    const SizedBox(height: 15),
                                    nameField,
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    SizedBox(
                                      height: 30,
                                      child: Row(
                                        children: [
                                          const Padding(
                                            padding: EdgeInsets.only(
                                              right: 8,
                                            ),
                                            child: Text('Active'),
                                          ),
                                          Checkbox(
                                            value: isActive,
                                            onChanged: (value) {
                                              setState(() {
                                                isActive = value!;
                                                // ignore: avoid_print
                                                print(isActive);
                                              });
                                            },
                                            activeColor: Colors.green,
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(height: 20),
                                    createButton,
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        const Text(
                                          "Change your Mind?",
                                          style: TextStyle(
                                            color: Colors.black,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: const Text(
                                            "Cancel",
                                            style: TextStyle(
                                                color: Colors.redAccent,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  postDetailsToFirestore() async {
    // calling our firestore
    // calling our user model
    // sending these values
    final FirebaseAuth _auth = FirebaseAuth.instance;
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;

    CreateItemModel itemModel = CreateItemModel();

    // writing all the values

    itemModel.uid = user!.uid;
    itemModel.name = nameEditingController.text;
    itemModel.status = isActive;
// ignore: avoid_print
    print(isActive);
    await firebaseFirestore
        .collection("items")
        .doc(user.uid)
        .set(itemModel.toMap());
    Fluttertoast.showToast(msg: "Items Created Successfully");

    Navigator.pushAndRemoveUntil(
        (context),
        MaterialPageRoute(builder: (context) => const ItemSetting()),
        (route) => false);
  }
}
