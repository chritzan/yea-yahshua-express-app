import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ItemSetting extends StatefulWidget {
  static String route = 'item-setting';
  const ItemSetting({Key? key}) : super(key: key);

  @override
  _ItemSettingState createState() => _ItemSettingState();
}

class _ItemSettingState extends State<ItemSetting> {
  // string for displaying the error Message
  String? errorMessage;
  bool isLoading = false;
  List<String> type = ['Name'];
  List<String> unit = ['Name'];
  List<String> view = ['Active', 'Inactive'];
  List<String> department = ['Mobile', 'Creative'];
  String? typeItem = '';
  String? unitItem = '';
  String? viewItem = '';
  String? departmentItem = '';

  //form controller
  final itemNameController = TextEditingController();
  final unitNameController = TextEditingController();

  //boll for status
  bool itemIsActive = false;
  bool unitIsActive = false;

  final CollectionReference _itemsName =
      FirebaseFirestore.instance.collection('items');
  final CollectionReference _unitsName =
      FirebaseFirestore.instance.collection('units');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a item if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing item
  Future<void> _createOrUpdateItems(
      [DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      itemNameController.text = documentSnapshot['name'];
      // itemIsActive = documentSnapshot['status'].toString() as bool;
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: itemNameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                ),
                Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(
                        right: 8,
                      ),
                      child: Text('Active?'),
                    ),
                    Checkbox(
                      value: itemIsActive,
                      onChanged: (value) {
                        setState(() {
                          itemIsActive = value!;
                          // ignore: avoid_print
                          print(itemIsActive);
                        });
                      },
                      activeColor: Colors.green,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? name = itemNameController.text;
                    // final bool status = itemIsActive;

                    if (name != null) {
                      if (action == 'create') {
                        // Persist a new item to Firestore
                        await _itemsName.add({
                          "name": name,
                          // "status": status,
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text(
                                    'You have successfully created a item')));
                      }

                      if (action == 'update') {
                        // Update the item
                        await _itemsName.doc(documentSnapshot!.id).update({
                          "name": name,
                          // "status": status,
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text(
                                    'You have successfully updated a item')));
                      }

                      // Clear the text fields
                      itemNameController.text = '';
                      // itemIsActive = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleteing a items by id
  Future<void> _deleteItems(String uid) async {
    await _itemsName.doc(uid).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('You have successfully deleted a item')));
  }

  //unit function
  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a unit if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing unit
  Future<void> _createOrUpdateUnits(
      [DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      unitNameController.text = documentSnapshot['unitname'];
      // unitIsActive = documentSnapshot['status'].toString() as bool;
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: unitNameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                ),
                Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(
                        right: 8,
                      ),
                      child: Text('Active?'),
                    ),
                    Checkbox(
                      value: unitIsActive,
                      onChanged: (value) {
                        setState(() {
                          unitIsActive = value!;
                          // ignore: avoid_print
                          print(unitIsActive);
                        });
                      },
                      activeColor: Colors.green,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? unitname = unitNameController.text;
                    // final bool status = unitIsActive;

                    if (unitname != null) {
                      if (action == 'create') {
                        // Persist a new unit to Firestore
                        await _unitsName.add({
                          "unitname": unitname,
                          // "status": status,
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text(
                                    'You have successfully created a unit')));
                      }

                      if (action == 'update') {
                        // Update the unit
                        await _unitsName.doc(documentSnapshot!.id).update({
                          "unitname": unitname,
                          // "status": status,
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text(
                                    'You have successfully updated a unit')));
                      }

                      // Clear the text fields
                      unitNameController.text = '';
                      // itemIsActive = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleteing a unitss by id
  Future<void> _deleteUnits(String uid) async {
    await _unitsName.doc(uid).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('You have successfully deleted a unit')));
  }

  @override
  void initState() {
    typeItem = type[0];
    unitItem = unit[0];
    viewItem = view[0];
    departmentItem = department[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                pinned: true,
                floating: true,
                title: const Text(
                  "Item Setting",
                ),
                bottom: TabBar(
                  isScrollable: true,
                  indicatorWeight: 10,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(20), // Creates border
                      color: Colors.amber),
                  tabs: const [
                    Tab(
                      text: 'Item',
                    ),
                    Tab(
                      text: 'Unit of Measure',
                    ),
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
            children: <Widget>[
              //item
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Type'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: typeItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: type.map(typeMenuItem).toList(),
                                onChanged: (String? value) => setState(
                                      () {
                                        if (value != null) typeItem = value;
                                        // ignore: avoid_print
                                        print(typeItem);
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (String? value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                        // ignore: avoid_print
                                        print(viewItem);
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Search')),
                            const Spacer(),
                            ElevatedButton(
                                onPressed: () => _createOrUpdateItems(),
                                child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        buildSearchField(),
                        const SizedBox(
                          height: 10,
                        ),
                        //itemslist
                        SizedBox(
                          height: 300,
                          child: StreamBuilder(
                            stream: _itemsName.snapshots(),
                            builder: (context,
                                AsyncSnapshot<QuerySnapshot> streamSnapshot) {
                              if (streamSnapshot.hasData) {
                                return ListView.builder(
                                  itemCount: streamSnapshot.data!.docs.length,
                                  itemBuilder: (context, index) {
                                    final DocumentSnapshot documentSnapshot =
                                        streamSnapshot.data!.docs[index];
                                    return Card(
                                      margin: const EdgeInsets.all(10),
                                      child: ListTile(
                                        title: Center(
                                            child:
                                                Text(documentSnapshot['name'])),
                                        subtitle: Row(
                                          children: const [
                                            Text('Active? '),
                                            // Text(documentSnapshot['status']
                                            // .toString()),
                                          ],
                                        ),
                                        trailing: SizedBox(
                                          width: 100,
                                          child: Row(
                                            children: [
                                              // Press this button to edit a single product
                                              IconButton(
                                                  icon: const Icon(Icons.edit),
                                                  onPressed: () =>
                                                      _createOrUpdateItems(
                                                          documentSnapshot)),
                                              // This icon button is used to delete a single product
                                              IconButton(
                                                icon: const Icon(Icons.delete),
                                                onPressed: () {
                                                  setState(() {
                                                    showDialog<String>(
                                                      context: context,
                                                      builder: (context) =>
                                                          AlertDialog(
                                                        title: Row(
                                                          children: const [
                                                            Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(8.0),
                                                              child: Text(
                                                                'Warning',
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .red,
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons.warning,
                                                              color: Colors.red,
                                                            ),
                                                          ],
                                                        ),
                                                        content:
                                                            SingleChildScrollView(
                                                          child: Column(
                                                            children: const [
                                                              Text(
                                                                  'Do you want to delete this item?')
                                                            ],
                                                          ),
                                                        ),
                                                        actions: [
                                                          TextButton(
                                                            onPressed: () {
                                                              setState(() {
                                                                _deleteItems(
                                                                    documentSnapshot
                                                                        .id);
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              });
                                                            },
                                                            child: const Text(
                                                                'Yes'),
                                                          ),
                                                          TextButton(
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                            child: const Text(
                                                                'No'),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }

                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            },
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
              // unit of measure
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Unit'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: unitItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: unit.map(unitMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) unitItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (String? value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Search')),
                            const Spacer(),
                            ElevatedButton(
                                onPressed: () => _createOrUpdateUnits(),
                                child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        buildSearchField(),
                        const SizedBox(
                          height: 10,
                        ),
                        //unit-list
                        SizedBox(
                          height: 300,
                          child: StreamBuilder(
                            stream: _unitsName.snapshots(),
                            builder: (context,
                                AsyncSnapshot<QuerySnapshot> streamSnapshot) {
                              if (streamSnapshot.hasData) {
                                return ListView.builder(
                                  itemCount: streamSnapshot.data!.docs.length,
                                  itemBuilder: (context, index) {
                                    final DocumentSnapshot documentSnapshot =
                                        streamSnapshot.data!.docs[index];
                                    return Card(
                                      margin: const EdgeInsets.all(10),
                                      child: ListTile(
                                        title: Center(
                                            child: Text(
                                                documentSnapshot['unitname'])),
                                        subtitle: Row(
                                          children: const [
                                            Text('Active? '),
                                            // Text(documentSnapshot['status']
                                            //     .toString()),
                                          ],
                                        ),
                                        trailing: SizedBox(
                                          width: 100,
                                          child: Row(
                                            children: [
                                              // Press this button to edit a single product
                                              IconButton(
                                                  icon: const Icon(Icons.edit),
                                                  onPressed: () =>
                                                      _createOrUpdateUnits(
                                                          documentSnapshot)),
                                              // This icon button is used to delete a single product
                                              IconButton(
                                                icon: const Icon(Icons.delete),
                                                onPressed: () {
                                                  setState(() {
                                                    showDialog<String>(
                                                      context: context,
                                                      builder: (context) =>
                                                          AlertDialog(
                                                        title: Row(
                                                          children: const [
                                                            Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(8.0),
                                                              child: Text(
                                                                'Warning',
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .red,
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons.warning,
                                                              color: Colors.red,
                                                            ),
                                                          ],
                                                        ),
                                                        content:
                                                            SingleChildScrollView(
                                                          child: Column(
                                                            children: const [
                                                              Text(
                                                                  'Do you want to delete this unit?')
                                                            ],
                                                          ),
                                                        ),
                                                        actions: [
                                                          TextButton(
                                                            onPressed: () {
                                                              setState(() {
                                                                _deleteUnits(
                                                                    documentSnapshot
                                                                        .id);
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              });
                                                            },
                                                            child: const Text(
                                                                'Yes'),
                                                          ),
                                                          TextButton(
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                            child: const Text(
                                                                'No'),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }

                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            },
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  DropdownMenuItem<String> typeMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> viewMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> unitMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  Widget buildSearchField() {
    const color = Colors.white;

    return TextField(
      style: const TextStyle(color: color),
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        hintText: 'Search',
        hintStyle: const TextStyle(color: color),
        prefixIcon: const Icon(Icons.search, color: color),
        filled: true,
        fillColor: Colors.black,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
      ),
    );
  }
}
