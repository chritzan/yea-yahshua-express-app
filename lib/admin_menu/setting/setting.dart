import 'package:flutter/material.dart';
import 'package:yahshua_express_app/admin_menu/setting/item_setting.dart';
import 'package:yahshua_express_app/admin_menu/setting/user_setting_rov.dart';

class Setting extends StatefulWidget {
  static String route = 'item-setting';
  const Setting({Key? key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  final double opacity = 0.2;
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          //title: const Text('Setting'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Stack(
              children: <Widget>[
                Column(
                  children: [
                    const Text(
                      'Setting | General',
                      style: TextStyle(fontSize: 30),
                    ),
                    const SizedBox(
                      height: 120,
                    ),
                    const Text(
                      'Setting Types',
                      style: TextStyle(fontSize: 20),
                    ),
                    const Divider(
                      color: Colors.amber,
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      const UserSettingRov()));
                        },
                        child: const Text('User Setting')),
                    const Divider(
                      color: Colors.amber,
                    ),
                    TextButton(
                        onPressed: () {},
                        child: const Text('Scheduling Setting')),
                    const Divider(
                      color: Colors.amber,
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const ItemSetting()));
                        },
                        child: const Text('Items Setting')),
                    const Divider(
                      color: Colors.amber,
                    ),
                    TextButton(
                        onPressed: () {},
                        child: const Text('Activity Setting')),
                    const Divider(
                      color: Colors.amber,
                    ),
                    const SizedBox(height: 24),
                    const SizedBox(height: 24),
                    const Text('Powered by Yahshua Systech &'),
                    const Text('YAHSHUA Early Career Program Batch 5'),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      );
}
