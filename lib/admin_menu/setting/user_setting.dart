import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserSetting extends StatefulWidget {
  static String route = 'user-manager';
  const UserSetting({Key? key}) : super(key: key);

  @override
  _UserSettingState createState() => _UserSettingState();
}

class _UserSettingState extends State<UserSetting> {
  List<String> type = ['Name'];
  List<String> view = ['Active', 'Inactive'];
  List<String> department = ['Mobile', 'Creative'];
  List<String> position = ['Head', 'Manager'];
  List<String> location = ['All'];

  String? typeItem = '';
  String? viewItem = '';
  String? departmentItem = '';
  String? positionItem = '';
  String? locationItem = '';

  //form controller
  final userNameController = TextEditingController();

  //boll for status
  bool userIsActive = false;

  final CollectionReference _usersName =
      FirebaseFirestore.instance.collection('usersettingName');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a item if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing item
  // ignore: unused_element
  Future<void> _createOrUpdateItems(
      [DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      userNameController.text = documentSnapshot['usersetting'];
      // itemIsActive = documentSnapshot['status'].toString() as bool;
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: userNameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                ),
                Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(
                        right: 8,
                      ),
                      child: Text('Active?'),
                    ),
                    Checkbox(
                      value: userIsActive,
                      onChanged: (value) {
                        setState(() {
                          userIsActive = value!;
                          // ignore: avoid_print
                          print(userIsActive);
                        });
                      },
                      activeColor: Colors.green,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? usersettingname = userNameController.text;
                    // final bool status = itemIsActive;

                    if (usersettingname != null) {
                      if (action == 'create') {
                        // Persist a new item to Firestore
                        await _usersName.add({
                          "usersettingname": usersettingname,
                          // "status": status,
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text(
                                    'You have successfully created a item')));
                      }

                      if (action == 'update') {
                        // Update the item
                        await _usersName.doc(documentSnapshot!.id).update({
                          "usersettingname": usersettingname,
                          // "status": status,
                        });
                        ScaffoldMessenger.of(context).showSnackBar(
                            const SnackBar(
                                content: Text(
                                    'You have successfully updated a item')));
                      }

                      // Clear the text fields
                      userNameController.text = '';
                      // itemIsActive = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleteing a items by id
  // ignore: unused_element
  Future<void> _deleteUsers(String uid) async {
    await _usersName.doc(uid).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('You have successfully deleted a item')));
  }

  @override
  void initState() {
    typeItem = type[0];
    viewItem = view[0];
    departmentItem = department[0];
    positionItem = position[0];
    locationItem = location[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final TextEditingController filterController = TextEditingController();
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                pinned: true,
                floating: true,
                title: const Text(
                  "User Setting",
                ),
                bottom: TabBar(
                  isScrollable: true,
                  indicatorWeight: 10,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(20), // Creates border
                      color: Colors.amber),
                  tabs: const [
                    Tab(
                      text: 'User',
                    ),
                    Tab(
                      text: 'Department',
                    ),
                    Tab(
                      text: 'Position',
                    ),
                    Tab(
                      text: 'Location',
                    ),
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
            children: [
              //userTab
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Type'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: typeItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: type.map(typeMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) typeItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        const Text('Department'),
                        TextFormField(
                          cursorColor: Colors.amberAccent,
                          autofocus: false,
                          controller: filterController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return ("Please Enter Value");
                            }
                          },
                          onSaved: (value) {
                            filterController.text = value!;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            fillColor: Colors.lightBlue,
                            prefixIcon: const Icon(
                              Icons.account_tree_outlined,
                              color: Colors.blueAccent,
                            ),
                            contentPadding:
                                const EdgeInsets.fromLTRB(20, 15, 20, 15),
                            hintText: "Enter Department",
                            hoverColor: Colors.blueAccent,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            TextButton(
                                onPressed: () {},
                                child: const Text('Select All')),
                            TextButton(
                                onPressed: () {},
                                child: const Text('Deselect All')),
                          ],
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Search')),
                            const Spacer(),
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        buildSearchField(),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 300,
                          child: Card(
                            shape: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            margin: const EdgeInsets.all(20),
                            shadowColor: Colors.blue,
                            elevation: 10,
                            color: Colors.amber.shade300,
                            child: Center(
                              child: ListView(),
                            ),
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
              //departmentTab
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Department'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: departmentItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items:
                                    department.map(departmentMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) {
                                          departmentItem = value;
                                        }
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        const Text('Location'),
                        TextFormField(
                          cursorColor: Colors.amberAccent,
                          autofocus: false,
                          controller: filterController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return ("Please Enter Value");
                            }
                          },
                          onSaved: (value) {
                            filterController.text = value!;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            fillColor: Colors.lightBlue,
                            prefixIcon: const Icon(
                              Icons.account_tree_outlined,
                              color: Colors.blueAccent,
                            ),
                            contentPadding:
                                const EdgeInsets.fromLTRB(20, 15, 20, 15),
                            hintText: "Enter Location",
                            hoverColor: Colors.blueAccent,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            TextButton(
                                onPressed: () {},
                                child: const Text('selectAll')),
                            TextButton(
                                onPressed: () {},
                                child: const Text('unselectAll')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Search')),
                            const Spacer(),
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        buildSearchField(),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 300,
                          child: Card(
                            shape: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            margin: const EdgeInsets.all(20),
                            shadowColor: Colors.blue,
                            elevation: 10,
                            color: Colors.amber.shade300,
                            child: Center(
                              child: ListView(),
                            ),
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
              //positionTab
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Position'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: positionItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: position.map(positionMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) positionItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        TextFormField(
                          cursorColor: Colors.amberAccent,
                          autofocus: false,
                          controller: filterController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return ("Please Enter Value");
                            }
                          },
                          onSaved: (value) {
                            filterController.text = value!;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            fillColor: Colors.lightBlue,
                            prefixIcon: const Icon(
                              Icons.account_tree_outlined,
                              color: Colors.blueAccent,
                            ),
                            contentPadding:
                                const EdgeInsets.fromLTRB(20, 15, 20, 15),
                            hintText: "Search",
                            hoverColor: Colors.blueAccent,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 300,
                          child: Card(
                            shape: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            margin: const EdgeInsets.all(20),
                            shadowColor: Colors.blue,
                            elevation: 10,
                            color: Colors.amber.shade300,
                            child: Center(
                              child: ListView(),
                            ),
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
              //locationTab
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Location'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: locationItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: location.map(locationMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) locationItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        TextFormField(
                          cursorColor: Colors.amberAccent,
                          autofocus: false,
                          controller: filterController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return ("Please Enter Value");
                            }
                          },
                          onSaved: (value) {
                            filterController.text = value!;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            fillColor: Colors.lightBlue,
                            prefixIcon: const Icon(
                              Icons.account_tree_outlined,
                              color: Colors.blueAccent,
                            ),
                            contentPadding:
                                const EdgeInsets.fromLTRB(20, 15, 20, 15),
                            hintText: "Search",
                            hoverColor: Colors.blueAccent,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        buildSearchField(),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 300,
                          child: Card(
                            shape: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            margin: const EdgeInsets.all(20),
                            shadowColor: Colors.blue,
                            elevation: 10,
                            color: Colors.amber.shade300,
                            child: Center(
                              child: ListView(),
                            ),
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  DropdownMenuItem<String> typeMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> locationMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> viewMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> departmentMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> positionMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  Widget buildSearchField() {
    const color = Colors.white;

    return TextField(
      style: const TextStyle(color: color),
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        hintText: 'Search',
        hintStyle: const TextStyle(color: color),
        prefixIcon: const Icon(Icons.search, color: color),
        filled: true,
        fillColor: Colors.black,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
      ),
    );
  }
}
