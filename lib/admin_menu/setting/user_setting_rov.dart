import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class UserSettingRov extends StatefulWidget {
  static String route = 'user-manager-rov';
  const UserSettingRov({Key? key}) : super(key: key);

  @override
  _UserSettingRovState createState() => _UserSettingRovState();
}

class _UserSettingRovState extends State<UserSettingRov> {
  List<String> type = ['Name'];
  List<String> view = ['Active', 'Inactive'];
  List<String> department = ['Mobile', 'Creative'];
  List<String> position = ['Head', 'Manager'];
  List<String> location = ['All'];

  String? typeItem = '';
  String? viewItem = '';
  String? userSettingItem = '';
  String? departmentItem = '';
  String? positionItem = '';
  String? locationItem = '';

  final userSettingNameController = TextEditingController();
  final departmentNameController = TextEditingController();
  final locationNameController = TextEditingController();
  final positionNameController = TextEditingController();

  bool isUserSettingActive = false;
  bool isDepartmentActive = false;
  bool isLocationActive = false;
  bool isPositionActive = false;

  final CollectionReference _userSettingName =
      FirebaseFirestore.instance.collection('usersetting');

  final CollectionReference _departmentName =
      FirebaseFirestore.instance.collection('department');

  final CollectionReference _locationName =
      FirebaseFirestore.instance.collection('location');

  final CollectionReference _positionName =
      FirebaseFirestore.instance.collection('position');

  Future<void> _createOrUpdateUserSettingName(
      [DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      userSettingNameController.text = documentSnapshot['usersettingname'];
      //isUserSettingActive = documentSnapshot ['status'].toString() as bool;
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: userSettingNameController,
                  decoration: const InputDecoration(labelText: 'User'),
                ),
                Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(
                        right: 8,
                      ),
                      child: Text('Active?'),
                    ),
                    Checkbox(
                      value: isUserSettingActive,
                      onChanged: (value) {
                        setState(() {
                          isUserSettingActive = value!;
                          // ignore: avoid_print
                          print(isUserSettingActive);
                        });
                      },
                      activeColor: Colors.green,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? usersettingname =
                        userSettingNameController.text;
                    //final bool status = isUserSettingActive;

                    if (usersettingname != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _userSettingName.add({
                          "usersettingname": usersettingname,
                          //"status": status,
                        });
                      }

                      if (action == 'update') {
                        // Update the product
                        await _userSettingName
                            .doc(documentSnapshot!.id)
                            .update({
                          "usersettingname": usersettingname,
                          //"status": status,
                        });
                      }

                      userSettingNameController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleteing a user by id
  Future<void> _deleteUserSettingName(String uid) async {
    await _userSettingName.doc(uid).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('You have successfully deleted a user')));
  }

  Future<void> _createOrUpdateDepartmentName(
      [DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      departmentNameController.text = documentSnapshot['departmentname'];
      //isDepartmentActive = documentSnapshot ['status'].toString() as bool;
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: departmentNameController,
                  decoration: const InputDecoration(labelText: 'Department'),
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? departmentName =
                        departmentNameController.text;
                    //final bool status = isDepartmentActive;

                    if (departmentName != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _departmentName.add({
                          "departmentname": departmentName,
                          //"status": status,
                        });
                      }

                      if (action == 'update') {
                        // Update the product
                        await _departmentName.doc(documentSnapshot!.id).update({
                          "departmentname": departmentName,
                          //"status": status,
                        });
                      }

                      departmentNameController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleteing a user by id
  Future<void> _deleteDepartmentName(String uid) async {
    await _departmentName.doc(uid).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a department')));
  }

  Future<void> _createOrUpdatePositionName(
      [DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      positionNameController.text = documentSnapshot['positionname'];
      //isPositionActive = documentSnapshot ['status'].toString() as bool;
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: positionNameController,
                  decoration: const InputDecoration(labelText: 'Position'),
                ),
                Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(
                        right: 8,
                      ),
                      child: Text('Active?'),
                    ),
                    Checkbox(
                      value: isPositionActive,
                      onChanged: (value) {
                        setState(() {
                          isPositionActive = value!;
                          // ignore: avoid_print
                          print(isPositionActive);
                        });
                      },
                      activeColor: Colors.green,
                    ),
                  ],
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? positionname = positionNameController.text;
                    //final bool status = isLocationActive;

                    if (positionname != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _positionName.add({
                          "positionname": positionname,
                          //"status": status,
                        });
                      }

                      if (action == 'update') {
                        // Update the product
                        await _positionName.doc(documentSnapshot!.id).update({
                          "positionname": positionname,
                          //"status": status,
                        });
                      }

                      positionNameController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleteing a user by id
  Future<void> _deletePositionName(String uid) async {
    await _positionName.doc(uid).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a position')));
  }

  Future<void> _createOrUpdateLocationName(
      [DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      locationNameController.text = documentSnapshot['locationname'];
      // itemIsActive = documentSnapshot['status'].toString() as bool;
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: locationNameController,
                  decoration: const InputDecoration(labelText: 'Location'),
                ),
                Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(
                        right: 8,
                      ),
                      child: Text('Active?'),
                    ),
                    Checkbox(
                      value: isLocationActive,
                      onChanged: (value) {
                        setState(() {
                          isLocationActive = value!;
                          // ignore: avoid_print
                          print(isLocationActive);
                        });
                      },
                      activeColor: Colors.green,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? locationname = locationNameController.text;
                    // final bool status = itemIsActive;

                    if (locationname != null) {
                      if (action == 'create') {
                        // Persist a new item to Firestore
                        await _locationName.add({
                          "locationname": locationname,
                          // "status": status,
                        });
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text(
                                'You have successfully created a location')));
                      }

                      if (action == 'update') {
                        // Update the item
                        await _locationName.doc(documentSnapshot!.id).update({
                          "locationname": locationname,
                          // "status": status,
                        });
                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                            content: Text(
                                'You have successfully updated a location')));
                      }

                      // Clear the text fields
                      locationNameController.text = '';
                      // itemIsActive = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleteing a user by id
  Future<void> _deleteLocationName(String uid) async {
    await _locationName.doc(uid).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text('You have successfully deleted a location')));
  }

  @override
  void initState() {
    typeItem = type[0];
    viewItem = view[0];
    departmentItem = department[0];
    positionItem = position[0];
    locationItem = location[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final TextEditingController filterController = TextEditingController();
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                pinned: true,
                floating: true,
                title: const Text(
                  "User Setting",
                ),
                bottom: TabBar(
                  isScrollable: true,
                  indicatorWeight: 10,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(20), // Creates border
                      color: Colors.amber),
                  tabs: const [
                    Tab(
                      text: 'User',
                    ),
                    Tab(
                      text: 'Department',
                    ),
                    Tab(
                      text: 'Position',
                    ),
                    Tab(
                      text: 'Location',
                    ),
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
            children: [
              //userTab
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Type'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: typeItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: type.map(typeMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) typeItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        const Text('Department'),
                        TextFormField(
                          cursorColor: Colors.amberAccent,
                          autofocus: false,
                          controller: filterController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return ("Please Enter Value");
                            }
                          },
                          onSaved: (value) {
                            filterController.text = value!;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            fillColor: Colors.lightBlue,
                            prefixIcon: const Icon(
                              Icons.account_tree_outlined,
                              color: Colors.blueAccent,
                            ),
                            contentPadding:
                                const EdgeInsets.fromLTRB(20, 15, 20, 15),
                            hintText: "Enter Department",
                            hoverColor: Colors.blueAccent,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            TextButton(
                                onPressed: () {},
                                child: const Text('Select All')),
                            TextButton(
                                onPressed: () {},
                                child: const Text('Deselect All')),
                          ],
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Search')),
                            const Spacer(),
                            ElevatedButton(
                                onPressed: () {
                                  _createOrUpdateUserSettingName();
                                },
                                child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        buildSearchField(),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 300,
                          child: StreamBuilder(
                            stream: _userSettingName.snapshots(),
                            builder: (context,
                                AsyncSnapshot<QuerySnapshot> streamSnapshot) {
                              if (streamSnapshot.hasData) {
                                return ListView.builder(
                                  itemCount: streamSnapshot.data!.docs.length,
                                  itemBuilder: (context, index) {
                                    final DocumentSnapshot documentSnapshot =
                                        streamSnapshot.data!.docs[index];
                                    return Card(
                                      margin: const EdgeInsets.all(10),
                                      child: ListTile(
                                        title: Center(
                                            child: Column(
                                          children: [
                                            Text(documentSnapshot[
                                                'usersettingname']),
                                          ],
                                        )),
                                        subtitle: Center(
                                          child: Column(
                                            children: const [
                                              Text('Active? ')
                                              //Text(documentSnapshot['status'].toString()),
                                            ],
                                          ),
                                        ),
                                        trailing: SizedBox(
                                          width: 100,
                                          child: Row(
                                            children: [
                                              // Press this button to edit a single product
                                              IconButton(
                                                  icon: const Icon(Icons.edit),
                                                  onPressed: () =>
                                                      _createOrUpdateUserSettingName(
                                                          documentSnapshot)),
                                              // This icon button is used to delete a single product
                                              IconButton(
                                                icon: const Icon(Icons.delete),
                                                onPressed: () {
                                                  setState(() {
                                                    showDialog<String>(
                                                      context: context,
                                                      builder: (context) =>
                                                          AlertDialog(
                                                        title: Row(
                                                          children: const [
                                                            Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(8.0),
                                                              child: Text(
                                                                'Warning',
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .red,
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons.warning,
                                                              color: Colors.red,
                                                            ),
                                                          ],
                                                        ),
                                                        content:
                                                            SingleChildScrollView(
                                                          child: Column(
                                                            children: const [
                                                              Text(
                                                                  'Do you want to delete this user item?')
                                                            ],
                                                          ),
                                                        ),
                                                        actions: [
                                                          TextButton(
                                                            onPressed: () {
                                                              setState(() {
                                                                _deleteUserSettingName(
                                                                    documentSnapshot
                                                                        .id);
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              });
                                                            },
                                                            child: const Text(
                                                                'Yes'),
                                                          ),
                                                          TextButton(
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                            child: const Text(
                                                                'No'),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }

                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            },
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
              //departmentTab
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Department'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: departmentItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items:
                                    department.map(departmentMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) {
                                          departmentItem = value;
                                        }
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        const Text('Location'),
                        TextFormField(
                          cursorColor: Colors.amberAccent,
                          autofocus: false,
                          controller: filterController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return ("Please Enter Value");
                            }
                          },
                          onSaved: (value) {
                            filterController.text = value!;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            fillColor: Colors.lightBlue,
                            prefixIcon: const Icon(
                              Icons.account_tree_outlined,
                              color: Colors.blueAccent,
                            ),
                            contentPadding:
                                const EdgeInsets.fromLTRB(20, 15, 20, 15),
                            hintText: "Enter Location",
                            hoverColor: Colors.blueAccent,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            TextButton(
                                onPressed: () {},
                                child: const Text('selectAll')),
                            TextButton(
                                onPressed: () {},
                                child: const Text('unselectAll')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {}, child: const Text('Search')),
                            const Spacer(),
                            ElevatedButton(
                                onPressed: () {
                                  _createOrUpdateDepartmentName();
                                },
                                child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        buildSearchField(),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 300,
                          child: StreamBuilder(
                            stream: _departmentName.snapshots(),
                            builder: (context,
                                AsyncSnapshot<QuerySnapshot> streamSnapshot) {
                              if (streamSnapshot.hasData) {
                                return ListView.builder(
                                  itemCount: streamSnapshot.data!.docs.length,
                                  itemBuilder: (context, index) {
                                    final DocumentSnapshot documentSnapshot =
                                        streamSnapshot.data!.docs[index];
                                    return Card(
                                      margin: const EdgeInsets.all(10),
                                      child: ListTile(
                                        title: Center(
                                            child: Column(
                                          children: [
                                            Text(documentSnapshot[
                                                'departmentname']),
                                            //Text(documentSnapshot['status'].toString()),
                                          ],
                                        )),
                                        subtitle: Center(
                                          child: Column(
                                            children: const [
                                              Text('Active? ')
                                              //Text(documentSnapshot['status'].toString()),
                                            ],
                                          ),
                                        ),
                                        trailing: SizedBox(
                                          width: 100,
                                          child: Row(
                                            children: [
                                              // Press this button to edit a single product
                                              IconButton(
                                                  icon: const Icon(
                                                    Icons.edit,
                                                    color: Colors.green,
                                                  ),
                                                  onPressed: () =>
                                                      _createOrUpdateDepartmentName(
                                                          documentSnapshot)),
                                              // This icon button is used to delete a single product
                                              IconButton(
                                                icon: const Icon(
                                                  Icons.delete,
                                                  color: Colors.red,
                                                ),
                                                onPressed: () {
                                                  setState(() {
                                                    showDialog<String>(
                                                      context: context,
                                                      builder: (context) =>
                                                          AlertDialog(
                                                        title: Row(
                                                          children: const [
                                                            Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(8.0),
                                                              child: Text(
                                                                'Warning',
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .red,
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons.warning,
                                                              color: Colors.red,
                                                            ),
                                                          ],
                                                        ),
                                                        content:
                                                            SingleChildScrollView(
                                                          child: Column(
                                                            children: const [
                                                              Text(
                                                                  'Do you want to delete this department')
                                                            ],
                                                          ),
                                                        ),
                                                        actions: [
                                                          TextButton(
                                                            onPressed: () {
                                                              setState(() {
                                                                _deleteDepartmentName(
                                                                    documentSnapshot
                                                                        .id);
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              });
                                                            },
                                                            child: const Text(
                                                                'Yes'),
                                                          ),
                                                          TextButton(
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                            child: const Text(
                                                                'No'),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }

                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            },
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
              //positionTab
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Position'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: positionItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: position.map(positionMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) positionItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        TextFormField(
                          cursorColor: Colors.amberAccent,
                          autofocus: false,
                          controller: filterController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return ("Please Enter Value");
                            }
                          },
                          onSaved: (value) {
                            filterController.text = value!;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            fillColor: Colors.lightBlue,
                            prefixIcon: const Icon(
                              Icons.account_tree_outlined,
                              color: Colors.blueAccent,
                            ),
                            contentPadding:
                                const EdgeInsets.fromLTRB(20, 15, 20, 15),
                            hintText: "Search",
                            hoverColor: Colors.blueAccent,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {
                                  _createOrUpdatePositionName();
                                },
                                child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 300,
                          child: StreamBuilder(
                            stream: _positionName.snapshots(),
                            builder: (context,
                                AsyncSnapshot<QuerySnapshot> streamSnapshot) {
                              if (streamSnapshot.hasData) {
                                return ListView.builder(
                                  itemCount: streamSnapshot.data!.docs.length,
                                  itemBuilder: (context, index) {
                                    final DocumentSnapshot documentSnapshot =
                                        streamSnapshot.data!.docs[index];
                                    return Card(
                                      margin: const EdgeInsets.all(10),
                                      child: ListTile(
                                        title: Center(
                                            child: Column(
                                          children: [
                                            Text(documentSnapshot[
                                                'positionname']),
                                            //Text(documentSnapshot['status'].toString()),
                                          ],
                                        )),
                                        subtitle: Center(
                                          child: Column(
                                            children: const [
                                              Text('Active? '),
                                              //Text(documentSnapshot['status'].toString()),
                                            ],
                                          ),
                                        ),
                                        trailing: SizedBox(
                                          width: 100,
                                          child: Row(
                                            children: [
                                              // Press this button to edit a single product
                                              IconButton(
                                                  icon: const Icon(
                                                    Icons.edit,
                                                    color: Colors.green,
                                                  ),
                                                  onPressed: () =>
                                                      _createOrUpdatePositionName(
                                                          documentSnapshot)),
                                              // This icon button is used to delete a single product
                                              IconButton(
                                                icon: const Icon(
                                                  Icons.delete,
                                                  color: Colors.red,
                                                ),
                                                onPressed: () {
                                                  setState(() {
                                                    showDialog<String>(
                                                      context: context,
                                                      builder: (context) =>
                                                          AlertDialog(
                                                        title: Row(
                                                          children: const [
                                                            Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(8.0),
                                                              child: Text(
                                                                'Warning',
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .red,
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons.warning,
                                                              color: Colors.red,
                                                            ),
                                                          ],
                                                        ),
                                                        content:
                                                            SingleChildScrollView(
                                                          child: Column(
                                                            children: const [
                                                              Text(
                                                                  'Do you want to delete this position?')
                                                            ],
                                                          ),
                                                        ),
                                                        actions: [
                                                          TextButton(
                                                            onPressed: () {
                                                              setState(() {
                                                                _deletePositionName(
                                                                    documentSnapshot
                                                                        .id);
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              });
                                                            },
                                                            child: const Text(
                                                                'Yes'),
                                                          ),
                                                          TextButton(
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                            child: const Text(
                                                                'No'),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }

                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            },
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
              //locationTab
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 20,
                        ),
                        const Text('Location'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade200,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: locationItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: location.map(locationMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) locationItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const Text('View'),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.amber.shade100,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                                value: viewItem,
                                iconSize: 36,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: Colors.black,
                                ),
                                isExpanded: true,
                                items: view.map(viewMenuItem).toList(),
                                onChanged: (value) => setState(
                                      () {
                                        if (value != null) viewItem = value;
                                      },
                                    )),
                          ),
                        ),
                        const SizedBox(
                          height: 50,
                        ),
                        TextFormField(
                          cursorColor: Colors.amberAccent,
                          autofocus: false,
                          controller: filterController,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value!.isEmpty) {
                              return ("Please Enter Value");
                            }
                          },
                          onSaved: (value) {
                            filterController.text = value!;
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            fillColor: Colors.lightBlue,
                            prefixIcon: const Icon(
                              Icons.account_tree_outlined,
                              color: Colors.blueAccent,
                            ),
                            contentPadding:
                                const EdgeInsets.fromLTRB(20, 15, 20, 15),
                            hintText: "Search",
                            hoverColor: Colors.blueAccent,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            ElevatedButton(
                                onPressed: () {
                                  _createOrUpdateLocationName();
                                },
                                child: const Text('Create')),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        buildSearchField(),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 300,
                          child: StreamBuilder(
                            stream: _locationName.snapshots(),
                            builder: (context,
                                AsyncSnapshot<QuerySnapshot> streamSnapshot) {
                              if (streamSnapshot.hasData) {
                                return ListView.builder(
                                  itemCount: streamSnapshot.data!.docs.length,
                                  itemBuilder: (context, index) {
                                    final DocumentSnapshot documentSnapshot =
                                        streamSnapshot.data!.docs[index];
                                    return Card(
                                      margin: const EdgeInsets.all(10),
                                      child: ListTile(
                                        title: Center(
                                            child: Text(documentSnapshot[
                                                'locationname'])),
                                        subtitle: Row(
                                          children: const [
                                            Text('Active? '),
                                            // Text(documentSnapshot['status']
                                            // .toString()),
                                          ],
                                        ),
                                        trailing: SizedBox(
                                          width: 100,
                                          child: Row(
                                            children: [
                                              // Press this button to edit a single product
                                              IconButton(
                                                  icon: const Icon(Icons.edit),
                                                  onPressed: () =>
                                                      _createOrUpdateLocationName(
                                                          documentSnapshot)),
                                              // This icon button is used to delete a single product
                                              IconButton(
                                                icon: const Icon(Icons.delete),
                                                onPressed: () {
                                                  setState(() {
                                                    showDialog<String>(
                                                      context: context,
                                                      builder: (context) =>
                                                          AlertDialog(
                                                        title: Row(
                                                          children: const [
                                                            Padding(
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(8.0),
                                                              child: Text(
                                                                'Warning',
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .red,
                                                                ),
                                                              ),
                                                            ),
                                                            Icon(
                                                              Icons.warning,
                                                              color: Colors.red,
                                                            ),
                                                          ],
                                                        ),
                                                        content:
                                                            SingleChildScrollView(
                                                          child: Column(
                                                            children: const [
                                                              Text(
                                                                  'Do you want to delete this location')
                                                            ],
                                                          ),
                                                        ),
                                                        actions: [
                                                          TextButton(
                                                            onPressed: () {
                                                              setState(() {
                                                                _deleteLocationName(
                                                                    documentSnapshot
                                                                        .id);
                                                                Navigator.of(
                                                                        context)
                                                                    .pop();
                                                              });
                                                            },
                                                            child: const Text(
                                                                'Yes'),
                                                          ),
                                                          TextButton(
                                                            onPressed: () {
                                                              Navigator.of(
                                                                      context)
                                                                  .pop();
                                                            },
                                                            child: const Text(
                                                                'No'),
                                                          ),
                                                        ],
                                                      ),
                                                    );
                                                  });
                                                },
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }

                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            },
                          ),
                        ),
                        const SizedBox(height: 24),
                        const SizedBox(height: 24),
                        const Text('Powered by Yahshua Systech &'),
                        const Text('YAHSHUA Early Career Program Batch 5'),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  DropdownMenuItem<String> typeMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> locationMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> viewMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> departmentMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  DropdownMenuItem<String> positionMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      );

  Widget buildSearchField() {
    const color = Colors.white;

    return TextField(
      style: const TextStyle(color: color),
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        hintText: 'Search',
        hintStyle: const TextStyle(color: color),
        prefixIcon: const Icon(Icons.search, color: color),
        filled: true,
        fillColor: Colors.black,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
      ),
    );
  }
}
