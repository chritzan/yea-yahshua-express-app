import 'package:flutter/material.dart';

class ItemsPage extends StatefulWidget {
  static String route = 'items';
  const ItemsPage({Key? key}) : super(key: key);

  @override
  _ItemsPageState createState() => _ItemsPageState();
}

class _ItemsPageState extends State<ItemsPage> {
  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(
        top: 200,
      ),
      child: SizedBox(
        height: 50,
        child: Text(
          'Coming Soon items',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
            color: Colors.amber,
          ),
        ),
      ),
    );
  }
}
