import 'package:flutter/material.dart';

class HelpMenu extends StatefulWidget {
  static String route = 'help';
  const HelpMenu({Key? key}) : super(key: key);

  @override
  _HelpMenuState createState() => _HelpMenuState();
}

class _HelpMenuState extends State<HelpMenu> {
  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(
        top: 200,
      ),
      child: SizedBox(
        height: 50,
        child: Text(
          'Coming Soon Help',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
            color: Colors.amber,
          ),
        ),
      ),
    );
  }
}
