import 'package:flutter/material.dart';

class Scheduling extends StatefulWidget {
  static String route = 'scheduling';
  const Scheduling({Key? key}) : super(key: key);

  @override
  _SchedulingState createState() => _SchedulingState();
}

class _SchedulingState extends State<Scheduling> {
  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(
        top: 200,
      ),
      child: SizedBox(
        height: 50,
        child: Text(
          'Coming Soon Schedule',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
            color: Colors.amber,
          ),
        ),
      ),
    );
  }
}
