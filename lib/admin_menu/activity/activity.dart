import 'package:flutter/material.dart';

class Activity extends StatefulWidget {
  static String route = 'activity';
  const Activity({Key? key}) : super(key: key);

  @override
  _ActivityState createState() => _ActivityState();
}

class _ActivityState extends State<Activity> {
  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(
        top: 200,
      ),
      child: SizedBox(
        height: 50,
        child: Text(
          'Coming Soon Activity',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
            color: Colors.amber,
          ),
        ),
      ),
    );
  }
}
