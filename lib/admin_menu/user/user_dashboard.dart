//this is just a sample code for crud implementation

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
//form controller
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final departmentController = TextEditingController();
  final locationController = TextEditingController();
  final positionController = TextEditingController();
  final typeController = TextEditingController();
  final passwordController = TextEditingController();

  final CollectionReference _users =
      FirebaseFirestore.instance.collection('users');

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product
  Future<void> _createOrUpdate([DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      nameController.text = documentSnapshot['name'];
      emailController.text = documentSnapshot['email'];
      departmentController.text = documentSnapshot['department'];
      locationController.text = documentSnapshot['location'];
      positionController.text = documentSnapshot['position'];
      typeController.text = documentSnapshot['type'];
      passwordController.text = documentSnapshot['password'];
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: nameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                ),
                TextField(
                  controller: emailController,
                  decoration: const InputDecoration(labelText: 'Email'),
                ),
                TextField(
                  controller: departmentController,
                  decoration: const InputDecoration(labelText: 'Department'),
                ),
                TextField(
                  controller: locationController,
                  decoration: const InputDecoration(labelText: 'Location'),
                ),
                TextField(
                  controller: positionController,
                  decoration: const InputDecoration(labelText: 'Position'),
                ),
                TextField(
                  controller: typeController,
                  decoration: const InputDecoration(labelText: 'Type'),
                ),
                TextField(
                  controller: passwordController,
                  decoration: const InputDecoration(labelText: 'Password'),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? name = nameController.text;
                    final String? email = emailController.text;
                    final String? department = departmentController.text;
                    final String? location = locationController.text;
                    final String? position = positionController.text;
                    final String? type = typeController.text;
                    final String? password = passwordController.text;
                    if (name != null &&
                        email != null &&
                        department != null &&
                        location != null &&
                        position != null &&
                        type != null &&
                        password != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _users.add({
                          "name": name,
                          "email": email,
                          "department": department,
                          "location": location,
                          "position": position,
                          "type": type,
                          "password": password,
                        });
                      }

                      if (action == 'update') {
                        // Update the product
                        await _users.doc(documentSnapshot!.id).update({
                          "name": name,
                          "email": email,
                          "department": department,
                          "location": location,
                          "position": position,
                          "type": type,
                          "password": password,
                        });
                      }

                      // Clear the text fields
                      nameController.text = '';
                      emailController.text = '';
                      departmentController.text = '';
                      locationController.text = '';
                      positionController.text = '';
                      typeController.text = '';
                      passwordController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleteing a product by id
  Future<void> _deleteProduct(String uid) async {
    await _users.doc(uid).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('You have successfully deleted a user')));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('YEA Users'),
      ),
      // Using StreamBuilder to display all products from Firestore in real-time
      body: StreamBuilder(
        stream: _users.snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> streamSnapshot) {
          if (streamSnapshot.hasData) {
            return ListView.builder(
              itemCount: streamSnapshot.data!.docs.length,
              itemBuilder: (context, index) {
                final DocumentSnapshot documentSnapshot =
                    streamSnapshot.data!.docs[index];
                return Card(
                  margin: const EdgeInsets.all(10),
                  child: ListTile(
                    title: Center(child: Text(documentSnapshot['name'])),
                    subtitle: Center(
                      child: Column(
                        children: [
                          Text(documentSnapshot['email']),
                          Text(documentSnapshot['department']),
                          Text(documentSnapshot['location']),
                          Text(documentSnapshot['position']),
                          Text(documentSnapshot['type']),
                          Text(documentSnapshot['password']),
                        ],
                      ),
                    ),
                    trailing: SizedBox(
                      width: 100,
                      child: Row(
                        children: [
                          // Press this button to edit a single product
                          IconButton(
                              icon: const Icon(Icons.edit),
                              onPressed: () =>
                                  _createOrUpdate(documentSnapshot)),
                          // This icon button is used to delete a single product
                          IconButton(
                              icon: const Icon(Icons.delete),
                              onPressed: () =>
                                  _deleteProduct(documentSnapshot.id)),
                        ],
                      ),
                    ),
                  ),
                );
              },
            );
          }

          return const Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
      // Add new product
      floatingActionButton: FloatingActionButton(
        onPressed: () => _createOrUpdate(),
        child: const Icon(Icons.add),
      ),
    );
  }
}
