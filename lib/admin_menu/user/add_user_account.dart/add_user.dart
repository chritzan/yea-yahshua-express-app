import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:yahshua_express_app/admin_menu/user/user_accounts.dart';
import 'package:yahshua_express_app/models/create_user_model.dart';

class AddUserWidget extends StatefulWidget {
  const AddUserWidget({Key? key}) : super(key: key);
  static String route = "add-user";

  @override
  _AddUserWidgetState createState() => _AddUserWidgetState();
}

class _AddUserWidgetState extends State<AddUserWidget> {
  // bool
  bool _obscureText = true;

  // firebase auth
  final _auth = FirebaseAuth.instance;

  // string for displaying the error Message
  String? errorMessage;

  // our form key
  final _formKey = GlobalKey<FormState>();

  // editing Controllers
  final nameEditingController = TextEditingController();
  final departmentEditingController = TextEditingController();
  final emailEditingController = TextEditingController();
  final passwordEditingController = TextEditingController();
  final locationEditingController = TextEditingController();
  final positionEditingController = TextEditingController();
  final typeEditingController = TextEditingController();
  bool isLoading = false;

  final double opacity = 0.4;

  @override
  Widget build(BuildContext context) {
    //responsiveness
    final size = MediaQuery.of(context).size;
    //first name field
    final nameField = TextFormField(
      autofocus: false,
      controller: nameEditingController,
      keyboardType: TextInputType.name,
      validator: (value) {
        RegExp regex = RegExp(r'^.{3,}$');
        if (value!.isEmpty) {
          return ("Name is Required");
        }
        if (!regex.hasMatch(value)) {
          return ("Minimum 3 Characters");
        }
        return null;
      },
      onSaved: (value) {
        nameEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        prefixIcon:
            const Icon(Icons.account_circle, color: Colors.lightBlueAccent),
        contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
        labelText: "Name",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );

    //email field
    final emailField = TextFormField(
      autofocus: false,
      controller: emailEditingController,
      keyboardType: TextInputType.emailAddress,
      validator: (value) {
        if (value!.isEmpty) {
          return ("Please Enter Email");
        }
        //reg expression for email validation
        if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]").hasMatch(value)) {
          return ("Please Enter Valid Email");
        }
        return null;
      },
      onSaved: (value) {
        emailEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        prefixIcon: const Icon(Icons.mail, color: Colors.blue),
        contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
        labelText: "Email",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );

    //department
    final departmentField = TextFormField(
      textInputAction: TextInputAction.next,
      controller: departmentEditingController,
      autofocus: false,
      validator: (value) {
        if (value!.isEmpty) {
          return ("Department is Required");
        }
      },
      onSaved: (value) {
        departmentEditingController.text = value!;
      },
      decoration: InputDecoration(
        labelText: 'Department',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );

    //location
    final locationField = TextFormField(
      textInputAction: TextInputAction.next,
      controller: locationEditingController,
      autofocus: false,
      validator: (value) {
        if (value!.isEmpty) {
          return ("Location is Required");
        }
      },
      onSaved: (value) {
        locationEditingController.text = value!;
      },
      decoration: InputDecoration(
        labelText: 'Location',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );

    //position
    final positionField = TextFormField(
      textInputAction: TextInputAction.next,
      controller: positionEditingController,
      autofocus: false,
      validator: (value) {
        if (value!.isEmpty) {
          return ("Position is Required");
        }
      },
      onSaved: (value) {
        positionEditingController.text = value!;
      },
      decoration: InputDecoration(
        labelText: 'Position',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
    //type
    final typeField = TextFormField(
      textInputAction: TextInputAction.next,
      controller: typeEditingController,
      autofocus: false,
      validator: (value) {
        if (value!.isEmpty) {
          return ("Type is Required");
        }
      },
      onSaved: (value) {
        typeEditingController.text = value!;
      },
      decoration: InputDecoration(
        labelText: 'Type',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );

    //password field
    final passwordField = TextFormField(
      autofocus: false,
      controller: passwordEditingController,
      obscureText: _obscureText,
      validator: (value) {
        RegExp regex = RegExp(
            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');

        if (value!.isEmpty) {
          return ("Password is Required");
        }
        if (!regex.hasMatch(value)) {
          return ("Password must Consist at least one [Uppercase, lowercase, numeric number and special character]");
        }
      },
      onSaved: (value) {
        passwordEditingController.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        prefixIcon: const Icon(Icons.vpn_key, color: Colors.red),
        errorMaxLines: 4, // number of lines the error text would wrap
        suffixIcon: GestureDetector(
          onTap: () {
            setState(() {
              _obscureText = !_obscureText;
            });
          },
          child: Icon(
            _obscureText ? Icons.visibility : Icons.visibility_off,
          ),
        ),
        contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
        labelText: "Password",
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );

    //Sign Up Button
    final createButton = Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(30),
      color: Colors.amber.shade200,
      child: MaterialButton(
        padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
        minWidth: MediaQuery.of(context).size.width,
        onPressed: () {
          if (nameEditingController.text.isNotEmpty &&
              emailEditingController.text.isNotEmpty &&
              departmentEditingController.text.isNotEmpty &&
              locationEditingController.text.isNotEmpty &&
              positionEditingController.text.isNotEmpty &&
              typeEditingController.text.isNotEmpty &&
              passwordEditingController.text.isNotEmpty) {
            setState(() {
              isLoading = true;
            });
          }
          signUp(emailEditingController.text, passwordEditingController.text);
        },
        child: const Text(
          "Create",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );

    return MaterialApp(
      //
      theme: ThemeData.light().copyWith(
          //hintColor: Colors.blue,
          ),
      home: Scaffold(
        backgroundColor: Colors.white,
        body: isLoading
            ? Center(
                child: SizedBox(
                  height: size.height / 20,
                  width: size.height / 20,
                  child: const CircularProgressIndicator(
                    color: Colors.lightGreen,
                  ),
                ),
              )
            : Center(
                child: Stack(
                  children: [
                    Container(
                      constraints: const BoxConstraints.expand(),
                      decoration: BoxDecoration(
                        color: Colors.white.withOpacity(opacity),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20),
                        ),
                        border: Border.all(
                          width: 1.5,
                          color: Colors.white.withOpacity(0.7),
                        ),
                      ),
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            left: 10,
                            right: 10,
                            top: 60,
                          ),
                          child: Form(
                            key: _formKey,
                            child: Container(
                              //color: const Color.fromRGBO(63, 255, 255, 90),
                              decoration: BoxDecoration(
                                color: Colors.white.withOpacity(opacity),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(20),
                                ),
                                border: Border.all(
                                  width: 1.5,
                                  color: Colors.white.withOpacity(0.7),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    const Text(
                                      "Create User",
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.amber,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    const SizedBox(height: 15),
                                    nameField,

                                    const SizedBox(height: 20),
                                    emailField,
                                    const SizedBox(height: 20),
                                    departmentField,
                                    const SizedBox(height: 20),
                                    locationField,
                                    const SizedBox(height: 20),
                                    positionField,
                                    const SizedBox(height: 20),
                                    typeField,
                                    const SizedBox(height: 20),
                                    passwordField,

                                    const SizedBox(height: 20),
                                    createButton,
                                    const SizedBox(
                                      height: 30,
                                    ),
                                    //const SizedBox(height: 15),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        const Text(
                                          "Change your Mind?",
                                          style: TextStyle(
                                            color: Colors.black,
                                          ),
                                        ),
                                        GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: const Text(
                                            "Cancel",
                                            style: TextStyle(
                                                color: Colors.redAccent,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  void signUp(String email, String password) async {
    if (_formKey.currentState!.validate()) {
      try {
        await _auth
            .createUserWithEmailAndPassword(email: email, password: password)
            .then((value) => {postDetailsToFirestore()})
            .catchError((e) {
          Fluttertoast.showToast(msg: e!.message);
        });
      } on FirebaseAuthException catch (error) {
        switch (error.code) {
          case "invalid-email":
            errorMessage = "Your email address appears to be malformed.";
            break;
          case "wrong-password":
            errorMessage = "Your password is wrong.";
            break;
          case "user-not-found":
            errorMessage = "User with this email doesn't exist.";
            break;
          case "user-disabled":
            errorMessage = "User with this email has been disabled.";
            break;
          case "too-many-requests":
            errorMessage = "Too many requests";
            break;
          case "operation-not-allowed":
            errorMessage = "Signing in with Email and Password is not enabled.";
            break;
          default:
            errorMessage = "An undefined Error happened.";
        }
        Fluttertoast.showToast(msg: errorMessage!);
        Text(error.code);
      }
    }
  }

  postDetailsToFirestore() async {
    // calling our firestore
    // calling our user model
    // sending these values
    final FirebaseAuth _auth = FirebaseAuth.instance;
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _auth.currentUser;

    CreateUserAccount userModel = CreateUserAccount();

    // writing all the values
    userModel.email = user!.email;
    userModel.uid = user.uid;
    userModel.name = nameEditingController.text;
    userModel.department = departmentEditingController.text;
    userModel.location = locationEditingController.text;
    userModel.position = positionEditingController.text;
    userModel.type = typeEditingController.text;
    userModel.password = passwordEditingController.text;

    await firebaseFirestore
        .collection("users")
        .doc(user.uid)
        .set(userModel.toMap());
    Fluttertoast.showToast(msg: "Account Created Successfully");

    Navigator.pushAndRemoveUntil(
        (context),
        MaterialPageRoute(builder: (context) => const AccountHome()),
        (route) => false);
  }
}
