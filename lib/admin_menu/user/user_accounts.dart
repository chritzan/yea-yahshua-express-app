import 'package:flutter/material.dart';
import 'package:yahshua_express_app/admin_menu/user/add_user_account.dart/add_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:yahshua_express_app/admin_menu/user/update_user_account/update_user_account.dart';

class AccountHome extends StatefulWidget {
  static String route = 'account-home';
  const AccountHome({Key? key}) : super(key: key);

  @override
  _AccountHomeState createState() => _AccountHomeState();
}

class _AccountHomeState extends State<AccountHome> {
//form controller
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final departmentController = TextEditingController();
  final locationController = TextEditingController();
  final positionController = TextEditingController();
  final typeController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  final CollectionReference _users =
      FirebaseFirestore.instance.collection('users');

  bool isUserActive = false;

  // This function is triggered when the floatting button or one of the edit buttons is pressed
  // Adding a product if no documentSnapshot is passed
  // If documentSnapshot != null then update an existing product
  Future<void> _createOrUpdate([DocumentSnapshot? documentSnapshot]) async {
    String action = 'create';
    if (documentSnapshot != null) {
      action = 'update';
      nameController.text = documentSnapshot['name'];
      emailController.text = documentSnapshot['email'];
      departmentController.text = documentSnapshot['department'];
      locationController.text = documentSnapshot['location'];
      positionController.text = documentSnapshot['position'];
      typeController.text = documentSnapshot['type'];
      passwordController.text = documentSnapshot['password'];
    }

    await showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext ctx) {
          return Padding(
            padding: EdgeInsets.only(
                top: 20,
                left: 20,
                right: 20,
                // prevent the soft keyboard from covering text fields
                bottom: MediaQuery.of(ctx).viewInsets.bottom + 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: nameController,
                  decoration: const InputDecoration(labelText: 'Name'),
                ),
                TextField(
                  controller: emailController,
                  decoration: const InputDecoration(labelText: 'Email'),
                ),
                TextField(
                  controller: departmentController,
                  decoration: const InputDecoration(labelText: 'Department'),
                ),
                TextField(
                  controller: locationController,
                  decoration: const InputDecoration(labelText: 'Location'),
                ),
                TextField(
                  controller: positionController,
                  decoration: const InputDecoration(labelText: 'Position'),
                ),
                TextField(
                  controller: typeController,
                  decoration: const InputDecoration(labelText: 'Type'),
                ),
                TextField(
                  controller: passwordController,
                  decoration: const InputDecoration(labelText: 'Password'),
                ),
                Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(right: 8),
                      child: Text('Active: '),
                    ),
                    Checkbox(
                      value: isUserActive,
                      onChanged: (value) {
                        setState(() {
                          isUserActive = value!;
                          // ignore: avoid_print
                          print(isUserActive);
                        });
                      },
                      activeColor: Colors.green,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  child: Text(action == 'create' ? 'Create' : 'Update'),
                  onPressed: () async {
                    final String? name = nameController.text;
                    final String? email = emailController.text;
                    final String? department = departmentController.text;
                    final String? location = locationController.text;
                    final String? position = positionController.text;
                    final String? type = typeController.text;
                    final String? password = passwordController.text;
                    if (name != null &&
                        email != null &&
                        department != null &&
                        location != null &&
                        position != null &&
                        type != null &&
                        password != null) {
                      if (action == 'create') {
                        // Persist a new product to Firestore
                        await _users.add({
                          "name": name,
                          "email": email,
                          "department": department,
                          "location": location,
                          "position": position,
                          "type": type,
                          "password": password,
                        });
                      }

                      if (action == 'update') {
                        // Update the product
                        await _users.doc(documentSnapshot!.id).update({
                          "name": name,
                          "email": email,
                          "department": department,
                          "location": location,
                          "position": position,
                          "type": type,
                          "password": password,
                        });
                      }

                      // Clear the text fields
                      nameController.text = '';
                      emailController.text = '';
                      departmentController.text = '';
                      locationController.text = '';
                      positionController.text = '';
                      typeController.text = '';
                      passwordController.text = '';

                      // Hide the bottom sheet
                      Navigator.of(context).pop();
                    }
                  },
                )
              ],
            ),
          );
        });
  }

  // Deleteing a user by id
  Future<void> _deleteUsers(String uid) async {
    await _users.doc(uid).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('You have successfully deleted a user')));
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          backgroundColor: const Color(0xFFA1887F),
          centerTitle: true,
          title: const Text('User Accounts'),
        ),
        body: Center(
          child: SizedBox(
            height: double.maxFinite,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topRight,
                    child: TextButton(
                      child: const Text('Create',
                          style: TextStyle(
                            fontSize: 20,
                            backgroundColor: Colors.amber,
                          )),
                      onPressed: () {
                        Navigator.pushNamed(context, AddUserWidget.route);
                        // setState(() {
                        //   openDialog();
                        // });
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  buildSearchField(),
                  const SizedBox(
                    height: 20,
                  ),
                  filterField(),
                  const SizedBox(
                    height: 20,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 350,
                    child: StreamBuilder(
                      stream: _users.snapshots(),
                      builder: (context,
                          AsyncSnapshot<QuerySnapshot> streamSnapshot) {
                        if (streamSnapshot.hasData) {
                          return ListView.builder(
                            itemCount: streamSnapshot.data!.docs.length,
                            itemBuilder: (context, index) {
                              final DocumentSnapshot documentSnapshot =
                                  streamSnapshot.data!.docs[index];
                              return Card(
                                margin: const EdgeInsets.all(10),
                                child: ListTile(
                                  title: Center(
                                      child: Text(documentSnapshot['name'])),
                                  subtitle: Center(
                                    child: Column(
                                      children: [
                                        Text(documentSnapshot['email']),
                                        Text(documentSnapshot['department']),
                                        Text(documentSnapshot['location']),
                                        Text(documentSnapshot['position']),
                                        Text(documentSnapshot['type']),
                                        Text(documentSnapshot['password']),
                                      ],
                                    ),
                                  ),
                                  trailing: SizedBox(
                                    width: 100,
                                    child: Row(
                                      children: [
                                        // Press this button to edit a single product
                                        IconButton(
                                            icon: const Icon(
                                              Icons.edit,
                                              color: Colors.green,
                                            ),
                                            onPressed: () => _createOrUpdate(
                                                documentSnapshot)),
                                        // This icon button is used to delete a single product
                                        IconButton(
                                          icon: const Icon(
                                            Icons.delete,
                                            color: Colors.red,
                                          ),
                                          onPressed: () {
                                            setState(() {
                                              showDialog<String>(
                                                context: context,
                                                builder: (context) =>
                                                    AlertDialog(
                                                  title: Row(
                                                    children: const [
                                                      Padding(
                                                        padding:
                                                            EdgeInsets.all(8.0),
                                                        child: Text(
                                                          'Warning',
                                                          style: TextStyle(
                                                            color: Colors.red,
                                                          ),
                                                        ),
                                                      ),
                                                      Icon(
                                                        Icons.warning,
                                                        color: Colors.red,
                                                      ),
                                                    ],
                                                  ),
                                                  content:
                                                      SingleChildScrollView(
                                                    child: Column(
                                                      children: const [
                                                        Text(
                                                            'Do you want to delete this user?')
                                                      ],
                                                    ),
                                                  ),
                                                  actions: [
                                                    TextButton(
                                                      onPressed: () {
                                                        setState(() {
                                                          _deleteUsers(
                                                              documentSnapshot
                                                                  .id);
                                                          Navigator.of(context)
                                                              .pop();
                                                        });
                                                      },
                                                      child: const Text('Yes'),
                                                    ),
                                                    TextButton(
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      child: const Text('No'),
                                                    ),
                                                  ],
                                                ),
                                              );
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        }

                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text('Powered by Yahshua Systech &'),
                  const Text('YAHSHUA Early Career Program Batch 5'),
                ],
              ),
            ),
          ),
        ),
      );

  TableRow buildRow(List<String> cells) => TableRow(
      children: cells
          .map((cell) => Padding(
                padding: const EdgeInsets.all(12),
                child: Center(child: Text(cell)),
              ))
          .toList());

  Widget buildSearchField() {
    const color = Colors.white;

    return TextField(
      style: const TextStyle(color: color),
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        hintText: 'Search',
        hintStyle: const TextStyle(color: color),
        prefixIcon: const Icon(Icons.search, color: color),
        filled: true,
        fillColor: Colors.black,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
      ),
    );
  }

  Widget filterField() {
    const color = Colors.white;

    return TextField(
      style: const TextStyle(color: color),
      decoration: InputDecoration(
        contentPadding:
            const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        hintText: 'Filter',
        hintStyle: const TextStyle(color: color),
        prefixIcon: const Icon(Icons.filter_2_outlined, color: color),
        filled: true,
        fillColor: Colors.green,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(5),
          borderSide: BorderSide(color: color.withOpacity(0.7)),
        ),
      ),
    );
  }
  //end of class brachet
}
