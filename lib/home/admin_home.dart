import 'package:flutter/material.dart';
//import 'package:yahshua_express/admin_menu/navigation/dropdown_side_nav.dart';
import 'package:yahshua_express_app/navigation/admin_nav.dart';

class AdminHome extends StatefulWidget {
  static String route = 'admin-home';
  const AdminHome({Key? key}) : super(key: key);

  @override
  _AdminHomeState createState() => _AdminHomeState();
}

class _AdminHomeState extends State<AdminHome> {
  final double opacity = 0.2;
  @override
  Widget build(BuildContext context) => Scaffold(
        drawer: const NavigationDrawerWidget(),
        //drawer: const DropdownDrawerWidget(),
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Dashboard'),
          centerTitle: true,
        ),

        body: SingleChildScrollView(
          child: Center(
            child: Stack(
              children: <Widget>[
                Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      child: const Text('Delivery Statistics'),
                      decoration: BoxDecoration(
                        color: Colors.amber.withOpacity(opacity),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20),
                        ),
                        border: Border.all(
                          width: 1.5,
                          color: Colors.amber.withOpacity(0.2),
                        ),
                      ),
                      height: 150,
                      width: 200,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: const Text('Pick-up Statistics'),
                      decoration: BoxDecoration(
                        color: Colors.amber.withOpacity(opacity),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20),
                        ),
                        border: Border.all(
                          width: 1.5,
                          color: Colors.amber.withOpacity(0.2),
                        ),
                      ),
                      height: 150,
                      width: 200,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: const Text('Retrieve and Return Statistics'),
                      decoration: BoxDecoration(
                        color: Colors.amber.withOpacity(opacity),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20),
                        ),
                        border: Border.all(
                          width: 1.5,
                          color: Colors.amber.withOpacity(0.2),
                        ),
                      ),
                      height: 150,
                      width: 200,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: const Text('Runner Statistics'),
                      decoration: BoxDecoration(
                        color: Colors.amber.withOpacity(opacity),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20),
                        ),
                        border: Border.all(
                          width: 1.5,
                          color: Colors.amber.withOpacity(0.2),
                        ),
                      ),
                      height: 150,
                      width: 200,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
              ],
            ),
          ),
        ),
      );
}
