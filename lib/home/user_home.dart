import 'package:flutter/material.dart';
import 'package:yahshua_express_app/navigation/user_nav.dart';
import 'package:yahshua_express_app/user_menu/deliver/deliver.dart';
import 'package:yahshua_express_app/user_menu/pick_up/pick_up.dart';

import 'package:yahshua_express_app/user_menu/request/request.dart';
import 'package:yahshua_express_app/user_menu/retrieve&return/retrieve_return_menu.dart';

class UserScreen extends StatefulWidget {
  static String route = 'user-home';
  const UserScreen({Key? key}) : super(key: key);

  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  final double opacity = 0.2;
  @override
  Widget build(BuildContext context) => Scaffold(
        drawer: const UserNavigationWidget(),
        appBar: AppBar(
          backgroundColor: const Color(0xFFFFB74D),
          title: const Text('Home'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 150,
                        width: 150,
                        decoration: BoxDecoration(
                            color: Colors.amber.withOpacity(opacity),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Icon(
                              Icons.book,
                              size: 100,
                              color: Colors.amber,
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => const RequestPage(),
                                  ));
                                },
                                child: const Text('Book')),
                          ],
                        ),
                      ),
                      const Spacer(),
                      Container(
                        height: 150,
                        width: 150,
                        decoration: BoxDecoration(
                            color: Colors.amber.withOpacity(opacity),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          children: [
                            const Icon(
                              Icons.car_rental,
                              size: 100,
                              color: Colors.amber,
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        const DeliveryScreen(),
                                  ));
                                },
                                child: const Text('Deliver')),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Container(
                        height: 150,
                        width: 150,
                        decoration: BoxDecoration(
                            color: Colors.amber.withOpacity(opacity),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          children: [
                            const Icon(
                              Icons.arrow_back,
                              size: 100,
                              color: Colors.amber,
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        const RetrieveReturnMenu(),
                                  ));
                                },
                                child: const Text('Retrieve & Return')),
                          ],
                        ),
                      ),
                      const Spacer(),
                      Container(
                        height: 150,
                        width: 150,
                        decoration: BoxDecoration(
                            color: Colors.amber.withOpacity(opacity),
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          children: [
                            const Icon(
                              Icons.download,
                              size: 100,
                              color: Colors.amber,
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => const PickUp(),
                                  ));
                                },
                                child: const Text('Pick-Up')),
                          ],
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text("Version 0.1"),
                ],
              ),
            ),
          ),
        ),
      );
}
