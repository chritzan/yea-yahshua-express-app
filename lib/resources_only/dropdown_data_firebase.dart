// sample code
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
// Imports all Widgets included in [multiselect] package
import 'package:multiselect/multiselect.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class DiplayDataDropdown extends StatefulWidget {
  static String route = 'dislaydata-dropdown';
  const DiplayDataDropdown({Key? key}) : super(key: key);

  @override
  State<DiplayDataDropdown> createState() => _DiplayDataDropdownState();
}

class _DiplayDataDropdownState extends State<DiplayDataDropdown> {
  List<String> selected = [];
  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Dropdown Data from Firebase'),
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                DropDownMultiSelect(
                  onChanged: (List<String> x) {
                    setState(() {
                      selected = x;
                    });
                  },
                  options: const ['a', 'b', 'c', 'd'],
                  selectedValues: selected,
                  whenEmpty: 'Select Something',
                ),
              ],
            ),
          ),
        ),
      );
}

class DropdownSample extends StatefulWidget {
  const DropdownSample({Key? key}) : super(key: key);

  @override
  State<DropdownSample> createState() => _DropdownSampleState();
}

class _DropdownSampleState extends State<DropdownSample> {
  List<String> selectedItem = [];

  final _category = '';
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('items').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: CupertinoActivityIndicator(),
            );
          }
          var length = snapshot.data!.docs.length;
          final DocumentSnapshot ds = snapshot.data!.docs[length];

          return Container(
            padding: const EdgeInsets.only(bottom: 16.0),
            child: Row(
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: Container(
                      padding:
                          const EdgeInsets.fromLTRB(12.0, 10.0, 10.0, 10.0),
                      child: const Text(
                        "Choose Item/s",
                      ),
                    )),
                Expanded(
                  flex: 4,
                  child: InputDecorator(
                    decoration: const InputDecoration(
                      hintText: 'Choose Item/s',
                      hintStyle: TextStyle(
                        fontSize: 16.0,
                        fontFamily: "OpenSans",
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    // ignore: unnecessary_null_comparison
                    isEmpty: _category == null,
                    child: DropdownButton(
                      value: _category,
                      isDense: true,
                      // onChanged: (List<String> services) {
                      //   setState(() {
                      //     selectedItem = services;
                      //   });
                      // },
                      items:
                          snapshot.data!.docs.map((DocumentSnapshot document) {
                        return DropdownMenuItem<String>(
                            value: ds['name'],
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0)),
                              height: 100.0,
                              padding: const EdgeInsets.fromLTRB(
                                  10.0, 2.0, 10.0, 0.0),
                              //color: primaryColor,
                              child: Text(
                                ds['name'],
                              ),
                            ));
                      }).toList(), onChanged: (String? value) {  },
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
