import 'dart:io';

import 'package:camera/camera.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class BuildInCamera extends StatefulWidget {
  final String? userId;
  const BuildInCamera({
    Key? key,
    this.userId,
  }) : super(key: key);

  @override
  _BuildInCameraState createState() => _BuildInCameraState();
}

class _BuildInCameraState extends State<BuildInCamera> {
  List<CameraDescription>? cameras; //list out the camera available
  CameraController? controller; //controller for camera
  XFile? image; //for caputred image
  String? downloadURL;

  @override
  void initState() {
    loadCamera();
    super.initState();
  }

  loadCamera() async {
    cameras = await availableCameras();
    if (cameras != null) {
      controller = CameraController(cameras![0], ResolutionPreset.max);
      //cameras[0] = first camera, change to 1 to another camera

      controller!.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    } else {
      // ignore: avoid_print
      print("NO any camera found");
    }
  }

  // uploading the image to firebase cloudstore
  Future uploadImage(File image) async {
    final imgId = DateTime.now().millisecondsSinceEpoch.toString();
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    Reference reference = FirebaseStorage.instance
        .ref()
        .child('${widget.userId}/images')
        .child("post_$imgId");

    await reference.putFile(image);
    downloadURL = await reference.getDownloadURL();

    // cloud firestore
    await firebaseFirestore
        .collection("pickup")
        .doc(widget.userId)
        .collection("images")
        .add({'downloadURL': downloadURL}).whenComplete(
            () => showSnackBar("Image Uploaded", const Duration(seconds: 10)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Document Item"),
        backgroundColor: Colors.redAccent,
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
                height: 300,
                width: 400,
                child: controller == null
                    ? const Center(child: Text("Loading Camera..."))
                    : !controller!.value.isInitialized
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : CameraPreview(controller!)),
          ),
          SizedBox(
            // width: 300,
            // height: 350,
            //show captured image
            // padding: const EdgeInsets.all(30),
            child: image == null
                ? const Text("No image captured")
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.file(
                      File(image!.path),
                      // width: 400,
                      height: 200,
                    ),
                  ),
            //display captured image
          ),
          ElevatedButton.icon(
              onPressed: () async {
                try {
                  if (controller != null) {
                    //check if contrller is not null
                    if (controller!.value.isInitialized) {
                      //check if controller is initialized
                      image = await controller!.takePicture(); //capture image
                      setState(() {
                        //update UI
                      });
                    }
                  }
                } catch (e) {
                  // ignore: avoid_print
                  print(e); //show error
                }
              },
              icon: const Icon(Icons.camera),
              label: const Text('Capture')),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(onPressed: () {}, child: const Text('Upload')),
        ]),
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: () async {
      //     try {
      //       if (controller != null) {
      //         //check if contrller is not null
      //         if (controller!.value.isInitialized) {
      //           //check if controller is initialized
      //           image = await controller!.takePicture(); //capture image
      //           setState(() {
      //             //update UI
      //           });
      //         }
      //       }
      //     } catch (e) {
      //       // ignore: avoid_print
      //       print(e); //show error
      //     }
      //   },
      //   child: const Icon(Icons.camera),
      // ),
    );
  }

  showSnackBar(String snackText, Duration d) {
    final snackBar = SnackBar(content: Text(snackText), duration: d);
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
