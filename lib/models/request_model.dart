class RequestModel {
  String? uid;
  String? requestActivity;
  String? requestDateTime;
  String? requestItem;
  String? requestLocation;
  String? requestRunner;
  String? requester;

  RequestModel({
    this.uid,
    this.requestActivity,
    this.requestDateTime,
    this.requestItem,
    this.requestLocation,
    this.requestRunner,
    this.requester,
  });

  // receiving data from server
  factory RequestModel.fromMap(map) {
    return RequestModel(
      uid: map['uid'],
      requestActivity: map['requestActivity'],
      requestDateTime: map['requestDateTime'],
      requestItem: map['requestItem'],
      requestLocation: map['requestLocation'],
      requestRunner: map["requestRunner"],
      requester: map["requester"],
    );
  }

  // sending data to our server
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'requestActivity': requestActivity,
      'requestDateTime': requestDateTime,
      'requestItem': requestItem,
      'requestLocation': requestLocation,
      'requestRunner': requestRunner,
      'requester': requester,
    };
  }
}
