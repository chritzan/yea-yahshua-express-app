class CreateUserAccount {
  String? uid;
  String? name;
  String? email;
  String? department;
  String? location;
  String? position;
  String? type;
  String? password;
  bool? status;

  CreateUserAccount({
    this.uid,
    this.name,
    this.email,
    this.department,
    this.location,
    this.position,
    this.type,
    this.password,
    this.status,
  });

  // receiving data from server
  factory CreateUserAccount.fromMap(map) {
    return CreateUserAccount(
      uid: map['uid'],
      name: map['name'],
      email: map['email'],
      department: map['department'],
      location: map['location'],
      position: map["position"],
      type: map["type"],
      status: map["status"],
    );
  }

  // sending data to our server
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'name': name,
      'email': email,
      'department': department,
      'location': location,
      'position': position,
      'type': type,
      'password': password,
      'status': status,
    };
  }
}
