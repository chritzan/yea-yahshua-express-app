class CreateItemModel {
  String? uid;
  String? name;
  bool? status;

  CreateItemModel({this.uid, this.name, this.status});

  // receiving data from server
  factory CreateItemModel.fromMap(map) {
    return CreateItemModel(
      uid: map['uid'],
      name: map['name'],
      status: map['status'],
    );
  }

  // sending data to our server
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'name': name,
      'status': status,
    };
  }
}
