class CreateUnitModel {
  String? uid;
  String? name;
  bool? status;

  CreateUnitModel({this.uid, this.name, this.status});

  // receiving data from server
  factory CreateUnitModel.fromMap(map) {
    return CreateUnitModel(
      uid: map['uid'],
      name: map['name'],
      status: map['status'],
    );
  }

  // sending data to our server
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'name': name,
      'status': status,
    };
  }
}
