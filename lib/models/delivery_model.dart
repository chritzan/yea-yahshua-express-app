class DeliveryModel {
  String? uid;
  String? deliveryActivity;
  String? deliveryDateTime;
  String? deliverytItem;
  String? deliveryLocation;
  String? deliveryRunner;
  String? requester;
  String? deliveryReleaser;

  DeliveryModel({
    this.uid,
    this.deliveryActivity,
    this.deliveryDateTime,
    this.deliverytItem,
    this.deliveryLocation,
    this.deliveryRunner,
    this.requester,
    this.deliveryReleaser,
  });

  // receiving data from server
  factory DeliveryModel.fromMap(map) {
    return DeliveryModel(
      uid: map['uid'],
      deliveryActivity: map['deliveryActivity'],
      deliveryDateTime: map['deliveryDateTime'],
      deliverytItem: map['deliverytItem'],
      deliveryLocation: map['deliveryLocation'],
      deliveryRunner: map["deliveryRunner"],
      requester: map["requester"],
      deliveryReleaser: map["deliveryReleaser"],
    );
  }

  // sending data to our server
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'deliveryActivity': deliveryActivity,
      'deliveryDateTime': deliveryDateTime,
      'deliverytItem': deliverytItem,
      'deliveryLocation': deliveryLocation,
      'deliveryRunner': deliveryRunner,
      'requester': requester,
      'deliveryReleaser': deliveryReleaser,
    };
  }
}
