class PickUpModel {
  String? uid;
  String? pickUpActivity;
  String? pickUpDateTime;
  String? pickUptItem;
  String? pickUpLocation;
  String? pickUpRunner;
  String? requester;
  String? pickUpReleaser;
  String? itemAvailable;

  PickUpModel({
    this.uid,
    this.pickUpActivity,
    this.pickUpDateTime,
    this.pickUptItem,
    this.pickUpLocation,
    this.pickUpRunner,
    this.requester,
    this.pickUpReleaser,
    this.itemAvailable,
  });

  // receiving data from server
  factory PickUpModel.fromMap(map) {
    return PickUpModel(
      uid: map['uid'],
      pickUpActivity: map['pickUpActivity'],
      pickUpDateTime: map['pickUpDateTime'],
      pickUptItem: map['pickUptItem'],
      pickUpLocation: map['pickUpLocation'],
      pickUpRunner: map["pickUpRunner"],
      requester: map["requester"],
      pickUpReleaser: map["pickUpReleaser"],
      itemAvailable: map["itemAvailable"],
    );
  }

  // sending data to our server
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'pickUpActivity': pickUpActivity,
      'pickUpDateTime': pickUpDateTime,
      'pickUptItem': pickUptItem,
      'pickUpLocation': pickUpLocation,
      'pickUpRunner': pickUpRunner,
      'requester': requester,
      'pickUpReleaser': pickUpReleaser,
      'itemAvailable': itemAvailable,
    };
  }
}
